package com.simple_software.swim_masala.util;

import com.simple_software.swim_masala.model.*;

import java.time.Instant;

public class TestDataUtils {
    private static NetworkQueryResolver networkQueryResolver = new NetworkQueryResolver();

    public static Gossip createGossip(Peer aboutPeer, Peer fromPeer, Status status) {
        return createGossip(aboutPeer, fromPeer, status, 1);
    }

    public static Gossip createGossip(Peer aboutPeer, Peer fromPeer, Status status, int sequenceNumber) {
        return new GossipBuilder()
                .setAboutPeer(aboutPeer)
                .setBornAtTime(Instant.now())
                .setSequenceNumber(sequenceNumber)
                .setStatus(status)
                .setFromPeer(fromPeer)
                .createGossip();

    }

    public static Peer peer(String hostname, Instant incarnationInstant) {
        return new PeerBuilder()
                .setHostname(hostname)
                .setIncarnationInstant(incarnationInstant)
                .setType("LogServicer")
                .setName("LogServicer")
                .setPingReceiverPort(100)
                .setGossipReceiverPort(101)
                .setIncarnationNumber(1)
                .createPeer();
    }

    public static Peer peer(String hostname, int incarnationNumber, Instant incarnationInstant) {
        return new PeerBuilder()
                .setHostname(hostname)
                .setIncarnationInstant(incarnationInstant)
                .setType("LogServicer")
                .setName("LogServicer")
                .setPingReceiverPort(100)
                .setGossipReceiverPort(101)
                .setIncarnationNumber(incarnationNumber)
                .createPeer();
    }

    public static Peer peer(String hostname, Instant incarnationInstant, int pingReceiverPort) {
        return new PeerBuilder()
                .setHostname(hostname)
                .setIncarnationInstant(incarnationInstant)
                .setType("LogServicer")
                .setName("LogServicer")
                .setPingReceiverPort(pingReceiverPort)
                .setGossipReceiverPort(101)
                .setIncarnationNumber(1)
                .createPeer();
    }

    public static Peer selfAsPeer(Instant incarnationInstant) {
        return selfAsPeer(incarnationInstant, 100, 101);
    }

    public static Peer selfAsPeer(Instant incarnationInstant, int pingReceiverPort, int gossipReceiverPort) {
        return new PeerBuilder()
                .setHostname(networkQueryResolver.getHostname())
                .setIncarnationInstant(incarnationInstant)
                .setType("LogServicer")
                .setName("LogServicer")
                .setPingReceiverPort(pingReceiverPort)
                .setGossipReceiverPort(gossipReceiverPort)
                .setIncarnationNumber(1)
                .createPeer();
    }

    public static String hostname() {
        return networkQueryResolver.getHostname();
    }
}
