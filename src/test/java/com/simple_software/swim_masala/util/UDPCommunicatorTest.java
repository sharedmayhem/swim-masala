package com.simple_software.swim_masala.util;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.*;

public class UDPCommunicatorTest {

    private DatagramSocket datagramSocket;
    private UDPCommunicator udpCommunicator;
    private final NetworkQueryResolver networkQueryResolver = mock(NetworkQueryResolver.class);

    @Before
    public void setUp() {
        datagramSocket = Mockito.mock(DatagramSocket.class);
        when(networkQueryResolver.getDatagramSocket()).thenReturn(datagramSocket);
        udpCommunicator = new UDPCommunicator(10, networkQueryResolver);
    }

    @Test
    public void willSplitContentOnNewLineIntoMultiplePackets_ifContentIsGreaterThan1400Bytes() throws IOException {
        ArgumentCaptor<DatagramPacket> datagramPacketArgumentCaptor = ArgumentCaptor.forClass(DatagramPacket.class);
        udpCommunicator.send(new NetworkQueryResolver().getHostname(), 4567, "actual\ngossip\nfu\ntang\n");

        Mockito.verify(datagramSocket, times(3)).send(datagramPacketArgumentCaptor.capture());

        List<DatagramPacket> packets = datagramPacketArgumentCaptor.getAllValues();

        assertThat(packets.size(), equalTo(3));
        assertThat(new String(packets.get(0).getData()), equalTo("actual\n"));
        assertThat(new String(packets.get(1).getData()), equalTo("gossip\nfu\n"));
        assertThat(new String(packets.get(2).getData()), equalTo("tang\n"));
    }

}