package com.simple_software.swim_masala.util;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeDiagnosingMatcher;

import java.util.Collection;

public class CollectionMatcher<T> extends TypeSafeDiagnosingMatcher<Collection<T>> {
    private Collection<T> expected;

    public CollectionMatcher(Collection<T> expected) {
        this.expected = expected;
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("iterable over ")
                .appendText(expected.toString())
                .appendText(" in any order");
    }


    @Override
    protected boolean matchesSafely(Collection<T> actual, Description description) {

        return false;
    }
}
