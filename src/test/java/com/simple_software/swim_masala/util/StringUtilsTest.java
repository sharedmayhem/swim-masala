package com.simple_software.swim_masala.util;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.nio.InvalidMarkException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;

public class StringUtilsTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();
    @Test
    public void stringIsNotSplit_ifItDoesNotContainNewLine(){
        String testString = "This is a string of length";
        expectedException.expect(RuntimeException.class);

        StringUtils.splitStringOnNewLine(testString, 5);

    }

    @Test
    public void stringCannotBeSplit_whenNewLineCharacterDoesNotAppearWithinSplitSize(){
        String testString = "Gossip\nThe protocol is interesting\nHowever, the implementation is a pain. Lots of boundary cases to consider";
        expectedException.expect(InvalidMarkException.class);
        StringUtils.splitStringOnNewLine(testString, 40);
    }

    @Test
    public void stringIsSplitCorrectly(){
        String testString = "Gossip\nThe protocol is interesting\nHowever, the implementation \nis a pain. Lots of boundary \ncases to consider";
        List<String> splitStrings = StringUtils.splitStringOnNewLine(testString, 40);
        assertThat(splitStrings, Matchers.contains("Gossip\nThe protocol is interesting\n","However, the implementation \n","is a pain. Lots of boundary \n","cases to consider"));
    }
}