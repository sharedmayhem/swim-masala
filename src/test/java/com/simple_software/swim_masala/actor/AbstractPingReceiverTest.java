package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.supportactor.Pinger;
import com.simple_software.swim_masala.util.NetworkQueryResolver;
import com.simple_software.swim_masala.util.TCPCommunicator;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Duration;
import java.time.Instant;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static com.simple_software.swim_masala.util.TestDataUtils.selfAsPeer;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;

public abstract class AbstractPingReceiverTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    final NetworkQueryResolver networkQueryResolver = new NetworkQueryResolver();
    Instant peerIncarnationInstant = Instant.ofEpochSecond(1518733285);
    PingReceiver pingReceiver;
    TCPCommunicator tcpCommunicator;
    ServerSocket inboundServerSocket;
    PeerTracker remotePeerTracker;
    GossipCollector remoteGossipCollector;
    Pinger remotePinger;
    private ExecutorService pingReceiverService;
    protected IdentityPeerService identityPeerService;

    @Before
    public void setUp() {
        tcpCommunicator = new TCPCommunicator();
        pingReceiverService = Executors.newFixedThreadPool(2);
        inboundServerSocket = networkQueryResolver.getServerSocket();
        identityPeerService = new IdentityPeerService(selfAsPeer(peerIncarnationInstant, inboundServerSocket.getLocalPort(), -1));
        remotePeerTracker = new PeerTracker(identityPeerService, Duration.ofSeconds(0), peer -> {
        }, p -> {
        });
        remoteGossipCollector = mock(GossipCollector.class);
        remotePinger = mock(Pinger.class);
        pingReceiver = new PingReceiver(inboundServerSocket, pingReceiverService, remotePeerTracker, remoteGossipCollector, remotePinger);

    }

    @After
    public void tearDown() {
        pingReceiverService.shutdown();
    }


    Socket connectToPingReceiver() throws IOException {
        return new Socket(networkQueryResolver.getHostname(), inboundServerSocket.getLocalPort());
    }

    void failWhenRequestIs(String request, String expectedExceptionMessage) {
        try {
            Socket clientConnectionToServer = connectToPingReceiver();
            tcpCommunicator.writeLine(request, clientConnectionToServer);
            Socket serverConnectionToClient = inboundServerSocket.accept();
            pingReceiver.processSocket(serverConnectionToClient);
            fail();
        } catch (Exception e) {
            Assert.assertThat(e.getMessage(), Matchers.equalTo(expectedExceptionMessage));
        }
    }
}