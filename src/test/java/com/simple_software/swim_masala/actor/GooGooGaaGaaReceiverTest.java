package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.util.Pair;
import com.simple_software.swim_masala.util.TestDataUtils;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.mockito.Mockito.verify;

public class GooGooGaaGaaReceiverTest extends AbstractPingReceiverTest {

    @Test
    public void whenAnInvalidGooGooGaaGaaRequestIsReceived_thenFail() throws Exception {
        failWhenRequestIs("goo-goo:", "java.lang.RuntimeException: Invalid baby talk");
    }

    @Test
    public void whenGooGooGaaGaaRequestIsReceived_andIfThereAreNoPeers_thenSendSelf_andGossipCollectorIsUpdatedWithInfantPeer() throws Exception {
        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();
        tcpCommunicator.writeLine("goo-goo:" + localhostAsPeer().asString(), clientConnectionToServer);

        pingReceiver.processSocket(serverConnectionToClient);

        assertThat(tcpCommunicator.readLine(clientConnectionToServer.getInputStream()), Matchers.equalTo(remotePeerTracker.getIdentityPeer().asString() + ":ALIVE"));
        verify(remoteGossipCollector).gossip(localhostAsPeer(), Status.ALIVE);
    }

    @Test
    public void whenGooGooGaaGaaRequestIsReceived_thenSendAllPeers_updatePeerTracker_andUpdateGossip() throws Exception {
        updateRemotePeerTracker();
        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();

        tcpCommunicator.writeLine(String.format("goo-goo:%s", localhostAsPeer().asString()), clientConnectionToServer);
        pingReceiver.processSocket(serverConnectionToClient);

        assertThat(mapToPeerStatusPair(clientConnectionToServer),
                containsInAnyOrder(
                        new Pair<>(peer("host1"), Status.ALIVE),
                        new Pair<>(peer("host2"), Status.SUSPICIOUS),
                        new Pair<>(remotePeerTracker.getIdentityPeer(), Status.ALIVE)));
        verify(remoteGossipCollector).gossip(localhostAsPeer(), Status.ALIVE);
    }

    private List<Pair<Peer, Status>> mapToPeerStatusPair(Socket clientConnectionToServer) throws IOException {
        return tcpCommunicator
                .readLines(clientConnectionToServer.getInputStream())
                .stream()
                .map(Infant::mapToPeerStatusPair)
                .collect(Collectors.toList());
    }

    private Peer localhostAsPeer() {
        return TestDataUtils.selfAsPeer(peerIncarnationInstant);
    }


    private void updateRemotePeerTracker() {
        remotePeerTracker.add(peer("host1"), Status.ALIVE);
        remotePeerTracker.add(peer("host2"), Status.SUSPICIOUS);
    }

    private Peer peer(String hostname) {
        return TestDataUtils.peer(hostname, peerIncarnationInstant);
    }

}