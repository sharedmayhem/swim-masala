package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Gossip;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.util.TestDataUtils;
import com.simple_software.swim_masala.util.UDPCommunicator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.simple_software.swim_masala.util.TestDataUtils.peer;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class GossipDisseminatorTest {

    @Mock
    private UDPCommunicator udpCommunicator;

    @Mock
    private PeerTracker peerTracker;

    @Mock
    private GossipCollector gossipCollector;

    private GossipDisseminator gossipDisseminator;
    private final Instant peerIncarnationInstant = Instant.ofEpochSecond(1518527759);


    @Before
    public void setUp() throws Exception {
        when(gossipCollector.allTheLatestGossips()).thenReturn(createGossips());
        when(peerTracker.getGossipPeers()).thenReturn(Collections.singletonList(peer("host11", peerIncarnationInstant)));
        gossipDisseminator = new GossipDisseminator(udpCommunicator, peerTracker, gossipCollector, new IdentityPeerService(TestDataUtils.selfAsPeer(peerIncarnationInstant)));
    }

    @Test
    public void ifNoGossips_thenDoNotDisseminateAnything(){
        when(gossipCollector.allTheLatestGossips()).thenReturn(new ArrayList<>());

        gossipDisseminator.disseminate();

        verifyNoMoreInteractions(udpCommunicator);
    }

    @Test
    public void gossipIsDisseminatedToPairOfPeers(){
        when(gossipCollector.allTheLatestGossips()).thenReturn(createGossips());
        gossipDisseminator.disseminate();
        verify(udpCommunicator).send(any(String.class), eq(101), eq(gossipsAsString(createGossips(), "\n")));
    }

    @Test
    public void whenThereIsSuspiciousGossip_thenItIsDisseminatedToSuspiciousPeer_toGiveThePeerAChanceToRefute(){
        List<Gossip> gossips = createGossips();
        Gossip host4 = TestDataUtils.createGossip(peer("host4", peerIncarnationInstant), TestDataUtils.selfAsPeer(peerIncarnationInstant), Status.SUSPICIOUS, 1);
        gossips.add(host4);
        when(gossipCollector.allTheLatestGossips()).thenReturn(gossips);

        gossipDisseminator.disseminate();

        verify(udpCommunicator, times(1)).send(ArgumentMatchers.any(), eq(101), eq(gossipsAsString(gossips, "\n")));
        verify(udpCommunicator, times(1)).send(eq("host4"), eq(101), eq(host4.asString()));
    }

    @Test
    public void whenThereIsDeadGossip_thenItIsDisseminatedToDeadPeer_toGiveThePeerAChanceToRefute(){
        List<Gossip> gossips = createGossips();
        Gossip deadGossip = TestDataUtils.createGossip(peer("host4", peerIncarnationInstant), TestDataUtils.selfAsPeer(peerIncarnationInstant), Status.DEAD, 1);
        gossips.add(deadGossip);
        when(peerTracker.getIdentityPeer()).thenReturn(deadGossip.getFromPeer());
        when(gossipCollector.allTheLatestGossips()).thenReturn(gossips);
        when(gossipCollector.getGossip(deadGossip.getAboutPeer(), Status.DEAD, deadGossip.getFromPeer())).thenReturn(deadGossip);
        when(peerTracker.getADeadPeer()).thenReturn(Optional.of(deadGossip.getAboutPeer()));

        gossipDisseminator.disseminate();

        verify(udpCommunicator, times(1)).send(ArgumentMatchers.any(), eq(101), eq(gossipsAsString(gossips, "\n")));
        verify(udpCommunicator, times(1)).send(eq("host4"), eq(101), eq(deadGossip.asString()));
    }
    private List<Gossip> createGossips(){
        List<Gossip> gossips = new ArrayList<>();
        gossips.add(TestDataUtils.createGossip(peer("host1", peerIncarnationInstant), TestDataUtils.selfAsPeer(peerIncarnationInstant), Status.ALIVE, 1));
        gossips.add(TestDataUtils.createGossip(peer("host2", peerIncarnationInstant), TestDataUtils.selfAsPeer(peerIncarnationInstant), Status.ALIVE, 1));
        gossips.add(TestDataUtils.createGossip(peer("host3", peerIncarnationInstant), TestDataUtils.selfAsPeer(peerIncarnationInstant), Status.ALIVE, 1));
        return gossips;
    }

    private String gossipsAsString(List<Gossip> gossips, String delimiter){
        return gossips.stream().map(Gossip::asString).collect(Collectors.joining(delimiter));
    }
}