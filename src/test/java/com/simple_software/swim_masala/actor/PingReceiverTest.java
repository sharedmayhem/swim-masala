package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Status;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.net.Socket;
import java.time.Instant;

import static com.simple_software.swim_masala.util.TestDataUtils.peer;
import static org.hamcrest.MatcherAssert.assertThat;

public class PingReceiverTest extends AbstractPingReceiverTest{


    @Test
    public void ifUnknownRequestSent_thenFail() throws Exception {
        expectedException.expect(RuntimeException.class);
        expectedException.expectMessage("Unrecognized command received at ackRequest receiver");

        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();

        tcpCommunicator.writeLine("Aloha", clientConnectionToServer);
        pingReceiver.processSocket(serverConnectionToClient);
    }

    @Test
    public void whenAckRequestIsReceived_respondWithStatus() throws Exception {

        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();

        tcpCommunicator.writeLine("ack-req:" + peer("host1", Instant.now()).asString(), clientConnectionToServer);
        pingReceiver.processSocket(serverConnectionToClient);
        assertThat(tcpCommunicator.readLine(clientConnectionToServer.getInputStream()), Matchers.equalTo(String.format("%s:%s", networkQueryResolver.getHostname(), Status.ALIVE)));
    }







}