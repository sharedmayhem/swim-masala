package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Gossip;
import com.simple_software.swim_masala.model.NodeClock;
import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.NumberGenerator;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.util.NetworkQueryResolver;
import org.hamcrest.Matchers;
import org.hamcrest.collection.IsIterableContainingInAnyOrder;
import org.junit.Before;
import org.junit.Test;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.Charset;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

import static com.simple_software.swim_masala.util.TestDataUtils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

public class GossipReceiverTest {

    private final NetworkQueryResolver networkQueryResolver = new NetworkQueryResolver();
    private final Instant peerIncarnationInstant = Instant.ofEpochSecond(1518527759);
    private GossipReceiver gossipReceiver;
    private DatagramSocket inboundDatagramSocket;
    private PeerTracker peerTracker;
    private GossipCollector gossipCollector;
    private GossipDisseminator gossipDisseminator;
    private List<Gossip> allGossips;
    private Peer selfAsPeer;

    @Before
    public void setUp() {
        NodeClock nodeClock = mock(NodeClock.class);
        selfAsPeer = selfAsPeer(peerIncarnationInstant);
        IdentityPeerService identityPeerService = new IdentityPeerService(selfAsPeer);
        peerTracker = new PeerTracker(identityPeerService, Duration.ofSeconds(0), peer -> {
        }, p -> {
        });
        gossipCollector = new GossipCollector(20, new NumberGenerator(), nodeClock, peer -> peerTracker.addToDeadList(peer), identityPeerService);
        inboundDatagramSocket = networkQueryResolver.getDatagramSocket();
        gossipDisseminator = mock(GossipDisseminator.class);
        gossipReceiver = new GossipReceiver(inboundDatagramSocket, peerTracker, gossipCollector, Executors.newSingleThreadExecutor(), gossipDisseminator);

    }

    @Test
    public void onReceivingGossip_updatePeerTracker_andIfPeerHasNewStatus_thenAddToGossipCollector() {
        allGossips = createGossips(new NumberGenerator());
        DatagramPacket datagramPacket = getDatagramPacket(Gossip.concatenatedGossips(allGossips));

        gossipReceiver.processPacket(datagramPacket);

        assertPeers();

        List<Gossip> gossipsFromTheGossipCollector = gossipCollector.allTheLatestGossips();
        assertThat(gossipsFromTheGossipCollector.size(), Matchers.equalTo(allGossips.size()));
        assertThat(gossipsFromTheGossipCollector, IsIterableContainingInAnyOrder.containsInAnyOrder(allGossips.stream().map(Matchers::equalTo).collect(Collectors.toList())));

        //Send the same gossip again but this time, there should be no change in the gossip collector and the peer tracker
        datagramPacket = getDatagramPacket(Gossip.concatenatedGossips(Collections.singletonList(allGossips.get(0))));

        gossipReceiver.processPacket(datagramPacket);

        assertPeers();
        gossipsFromTheGossipCollector = gossipCollector.allTheLatestGossips();
        assertThat(gossipsFromTheGossipCollector.size(), Matchers.equalTo(allGossips.size()));
        assertThat(gossipsFromTheGossipCollector, IsIterableContainingInAnyOrder.containsInAnyOrder(allGossips.stream().map(Matchers::equalTo).collect(Collectors.toList())));
    }

    @Test
    public void onReceivingGossipAboutOneSelf_disseminateIfRequired() {
        NumberGenerator numberGenerator = new NumberGenerator();
        allGossips = createGossips(numberGenerator);
        Gossip gossipAboutOneSelf = createGossip(selfAsPeer, selfAsPeer, Status.SUSPICIOUS, numberGenerator.nextGossipSequenceNumber());
        allGossips.add(gossipAboutOneSelf);
        DatagramPacket datagramPacket = getDatagramPacket(Gossip.concatenatedGossips(allGossips));

        gossipReceiver.processPacket(datagramPacket);

        assertPeers();
        List<Gossip> actual = gossipCollector.allTheLatestGossips();
        List<Gossip> expected = createGossips(new NumberGenerator());
        assertThat(actual, IsIterableContainingInAnyOrder.containsInAnyOrder(expected.stream().map(Matchers::equalTo).collect(Collectors.toList())));
        verify(gossipDisseminator).refuteMyStatus();
    }

    @Test
    public void interruptingGossipReceiverWillStopTheReceiver() {
        allGossips = createGossips(new NumberGenerator());
        ExecutorService executorService = Executors.newFixedThreadPool(1);
        executorService.submit(() -> gossipReceiver.start());
        assertThat(executorService.isShutdown(), Matchers.equalTo(false));

        executorService.shutdownNow();

        assertThat(executorService.isShutdown(), Matchers.equalTo(true));
    }

    private void assertPeers() {
        assertThat(peerTracker.peers(), Matchers.contains(
                peer("host1", peerIncarnationInstant),
                peer("host3", peerIncarnationInstant),
                peer("host4", peerIncarnationInstant)));

        peerTracker.getADeadPeer().ifPresent(peer -> assertThat(peer, Matchers.equalTo(peer("host2", peerIncarnationInstant))));


    }

    private DatagramPacket getDatagramPacket(String concatenatedGossips) {
        byte[] bytes = concatenatedGossips.getBytes(Charset.forName("UTF-8"));
        return new DatagramPacket(bytes, bytes.length, networkQueryResolver.getLocalAddress(), inboundDatagramSocket.getLocalPort());
    }

    private List<Gossip> createGossips(NumberGenerator numberGenerator) {
        List<Gossip> gossips = new ArrayList<>();
        gossips.add(createGossip(peer("host1", peerIncarnationInstant), selfAsPeer, Status.ALIVE, numberGenerator.nextGossipSequenceNumber()));
        gossips.add(createGossip(peer("host2", peerIncarnationInstant), selfAsPeer, Status.DEAD, numberGenerator.nextGossipSequenceNumber()));
        gossips.add(createGossip(peer("host3", peerIncarnationInstant), selfAsPeer, Status.SUSPICIOUS, numberGenerator.nextGossipSequenceNumber()));
        gossips.add(createGossip(peer("host4", peerIncarnationInstant), selfAsPeer, Status.ALIVE, numberGenerator.nextGossipSequenceNumber()));
        return gossips;
    }

}