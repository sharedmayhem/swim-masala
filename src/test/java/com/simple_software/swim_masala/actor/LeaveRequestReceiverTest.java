package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.util.TestDataUtils;
import org.hamcrest.Matchers;
import org.junit.Test;
import org.mockito.Mockito;

import java.net.Socket;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

public class LeaveRequestReceiverTest extends AbstractPingReceiverTest {


    @Test
    public void whenAnInvalidIndirectPingRequestIsReceived_thenFail() throws Exception {
        failWhenRequestIs("bye-bye:", "java.lang.RuntimeException: Invalid leave request");
        failWhenRequestIs("bye-bye:123", "java.lang.RuntimeException: Invalid leave request");
        failWhenRequestIs(String.format("bye-bye:%s", networkQueryResolver.getHostname()), "java.lang.RuntimeException: Invalid leave request");
    }

    @Test
    public void whenLeaveRequestIsReceived_thenUpdatePeerTracker_andUpdateGossip() throws Exception {
        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();

        Peer peerLeaving = localhostAsPeer();
        tcpCommunicator.writeLine(String.format("bye-bye:%s", peerLeaving.asString()), clientConnectionToServer);
        pingReceiver.processSocket(serverConnectionToClient);

        assertThat(tcpCommunicator.readLine(clientConnectionToServer.getInputStream()),
                Matchers.equalTo("ok"));
        verify(remoteGossipCollector).gossip(peerLeaving, Status.DEAD);
    }

    @Test
    public void whenLeaveRequestIsReceivedTwice_thenIgnoreTheRedundantRequest() throws Exception {
        whenLeaveRequestIsReceived_thenUpdatePeerTracker_andUpdateGossip();
        Mockito.clearInvocations(remoteGossipCollector);

        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();

        Peer peerLeaving = localhostAsPeer();
        tcpCommunicator.writeLine(String.format("bye-bye:%s", peerLeaving.asString()), clientConnectionToServer);
        pingReceiver.processSocket(serverConnectionToClient);

        assertThat(tcpCommunicator.readLine(clientConnectionToServer.getInputStream()),
                Matchers.equalTo("ok"));
        verifyNoMoreInteractions(remoteGossipCollector);
    }

    @Test
    public void whenAPeerReJoinsAfterLeavingPreviously_thenUpdatePeerTracker_andUpdateGossip() throws Exception {
        whenLeaveRequestIsReceived_thenUpdatePeerTracker_andUpdateGossip();
        Mockito.clearInvocations(remoteGossipCollector);

        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();

        Peer rejoiningPeer = new IdentityPeerService(localhostAsPeer()).newIncarnation();
        tcpCommunicator.writeLine(String.format("goo-goo:%s", rejoiningPeer.asString()), clientConnectionToServer);
        pingReceiver.processSocket(serverConnectionToClient);

        assertThat(tcpCommunicator.readLine(clientConnectionToServer.getInputStream()),
                Matchers.equalTo(remotePeerTracker.getIdentityPeer().asString() + ":ALIVE"));
        verify(remoteGossipCollector).gossip(rejoiningPeer, Status.ALIVE);
    }

    @Test
    public void willRejectPhishingRequests() throws Exception {
        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();
        Peer peerLeaving = peer("host1");
        tcpCommunicator.writeLine(String.format("bye-bye:%s", peerLeaving.asString()), clientConnectionToServer);

        pingReceiver.processSocket(serverConnectionToClient);

        assertThat(tcpCommunicator.readLine(clientConnectionToServer.getInputStream()),
                Matchers.equalTo("fail"));

    }

    private Peer localhostAsPeer() {
        return TestDataUtils.selfAsPeer(peerIncarnationInstant);
    }

    private Peer peer(String hostname) {
        return TestDataUtils.peer(hostname, peerIncarnationInstant);
    }

}