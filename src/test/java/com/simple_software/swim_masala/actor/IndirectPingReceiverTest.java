package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.util.TestDataUtils;
import org.hamcrest.Matchers;
import org.junit.Test;

import java.net.Socket;
import java.time.Instant;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

public class IndirectPingReceiverTest extends AbstractPingReceiverTest {

    @Test
    public void whenAnInvalidIndirectPingRequestIsReceived_thenFail() throws Exception {
        failWhenRequestIs("ackRequest-req:", "java.lang.RuntimeException: Invalid indirect ackRequest request");
        failWhenRequestIs("ackRequest-req:123", "java.lang.RuntimeException: Invalid indirect ackRequest request");
        failWhenRequestIs(String.format("ackRequest-req:%s", networkQueryResolver.getHostname()), "java.lang.RuntimeException: Invalid indirect ackRequest request");
    }

    @Test
    public void ifTargetPeerIsAlive_thenReturnAckToRequestingPeer() throws Exception {
        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();
        Peer indirectPeerTarget = TestDataUtils.peer("host1", Instant.ofEpochSecond(13456789));
        when(remotePinger.ackRequest(indirectPeerTarget, identityPeerService.identityPeer())).thenReturn(true);
        tcpCommunicator.writeLine("ackRequest-req:" + indirectPeerTarget.asString(), clientConnectionToServer);

        pingReceiver.processSocket(serverConnectionToClient);

        assertThat(tcpCommunicator.readLine(clientConnectionToServer.getInputStream()), Matchers.equalTo("host1:ALIVE"));

    }

    @Test
    public void ifTargetPeerIsDead_thenReturnSuspiciousStatusToRequestingPeer() throws Exception {
        Socket clientConnectionToServer = connectToPingReceiver();
        Socket serverConnectionToClient = inboundServerSocket.accept();
        Peer indirectPeerTarget = TestDataUtils.peer("host1", Instant.ofEpochSecond(13456789));
        when(remotePinger.ackRequest(indirectPeerTarget, identityPeerService.identityPeer())).thenReturn(false);
        tcpCommunicator.writeLine("ackRequest-req:" + indirectPeerTarget.asString(), clientConnectionToServer);

        pingReceiver.processSocket(serverConnectionToClient);

        assertThat(tcpCommunicator.readLine(clientConnectionToServer.getInputStream()), Matchers.equalTo("host1:SUSPICIOUS"));

    }


}