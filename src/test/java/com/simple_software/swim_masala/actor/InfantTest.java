package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.supportactor.Pinger;
import com.simple_software.swim_masala.util.NetworkQueryResolver;
import com.simple_software.swim_masala.util.Pair;
import com.simple_software.swim_masala.util.TestDataUtils;
import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.*;

public class InfantTest {

    private static final int A_NONSENSICAL_PORT = -1;
    private NetworkQueryResolver networkQueryResolver;
    private ServerSocket pingReceiverSocketOnTargetPeer;
    private PeerTracker localPeerTracker;
    private GossipCollector localGossipCollector;
    private PingReceiver pingReceiver;
    private String localHostname;
    private Infant infant;
    private Instant peerIncarnationInstant = Instant.ofEpochSecond(1518908369);
    private Peer remotePeer;
    private final GossipDisseminator gossipDisseminator = mock(GossipDisseminator.class);


    @Before
    public void setUp() throws Exception {
        networkQueryResolver = new NetworkQueryResolver();
        localHostname = networkQueryResolver.getHostname();
        Peer selfAsPeer = TestDataUtils.selfAsPeer(peerIncarnationInstant);

        pingReceiverSocketOnTargetPeer = networkQueryResolver.getServerSocket();
        remotePeer = TestDataUtils.selfAsPeer(peerIncarnationInstant, pingReceiverSocketOnTargetPeer.getLocalPort(), A_NONSENSICAL_PORT);

        PeerTracker peerTrackerOnServer = mock(PeerTracker.class);
        when(peerTrackerOnServer.statusMap()).thenReturn(aPeerStatusMap());
        when(peerTrackerOnServer.getIdentityPeer()).thenReturn(remotePeer);

        pingReceiver = new PingReceiver(pingReceiverSocketOnTargetPeer, mock(ExecutorService.class), peerTrackerOnServer, mock(GossipCollector.class), mock(Pinger.class));

        localGossipCollector = mock(GossipCollector.class);
        IdentityPeerService identityPeerService = new IdentityPeerService(selfAsPeer);
        localPeerTracker = new PeerTracker(identityPeerService, Duration.ofSeconds(0), peer -> {
        }, p -> {
        });
        infant = new Infant(localGossipCollector, localPeerTracker, identityPeerService, gossipDisseminator);
    }

    @Test
    public void whenResponseReceived_willUpdatePeerTrackerAndGossipCollector_andDisseminateGossipAboutItself() throws Exception {
        Socket clientConnectionToServer = networkQueryResolver.getSocket(localHostname, pingReceiverSocketOnTargetPeer.getLocalPort());
        Collection<Peer> expectedPeersFromPingReceiver = new ArrayList<>();
        expectedPeersFromPingReceiver.add(remotePeer);
        expectedPeersFromPingReceiver.addAll(peers());

        CompletableFuture.runAsync(() -> {
            try {
                pingReceiver.processSocket(pingReceiverSocketOnTargetPeer.accept());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
        infant.buildWorldViewByAsking(clientConnectionToServer);



        assertThat(localPeerTracker.peers(), Matchers.containsInAnyOrder(collectionMatchers(expectedPeersFromPingReceiver).toArray(new Matcher[1])));

        ArgumentCaptor<Peer> peerArgumentCaptor = ArgumentCaptor.forClass(Peer.class);
        ArgumentCaptor<Status> statusArgumentCaptor = ArgumentCaptor.forClass(Status.class);
        verify(localGossipCollector, times(6)).gossip(peerArgumentCaptor.capture(), statusArgumentCaptor.capture());
        assertThat(peerArgumentCaptor.getAllValues(), Matchers.contains(collectionMatchers(expectedPeersFromPingReceiver).toArray(new Matcher[1])));
        assertThat(statusArgumentCaptor.getAllValues().stream().distinct().findFirst().orElse(Status.UNDEFINED), Matchers.equalTo(Status.ALIVE));
        verify(gossipDisseminator).tellFactsAboutMyOwnWellBeing();

    }

    private <T> Collection<Matcher<T>> collectionMatchers(Collection<T> collection) {
        return collection.stream().map(Matchers::equalTo).collect(Collectors.toList());
    }

    private Collection<Peer> peers() {
        return aPeerStatusMap().stream().map(Pair::getKey).collect(Collectors.toList());
    }

    private Collection<Pair<Peer, Status>> aPeerStatusMap() {
        Collection<Pair<Peer, Status>> statusMap = new ArrayList<>();
        statusMap.add(new Pair<>(TestDataUtils.peer("host1", peerIncarnationInstant), Status.ALIVE));
        statusMap.add(new Pair<>(TestDataUtils.peer("host2", peerIncarnationInstant), Status.ALIVE));
        statusMap.add(new Pair<>(TestDataUtils.peer("host3", peerIncarnationInstant), Status.ALIVE));
        statusMap.add(new Pair<>(TestDataUtils.peer("host4", peerIncarnationInstant), Status.ALIVE));
        statusMap.add(new Pair<>(TestDataUtils.peer("host5", peerIncarnationInstant), Status.ALIVE));
        return statusMap;
    }
}