package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Gossip;
import com.simple_software.swim_masala.model.NodeClock;
import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.*;
import com.simple_software.swim_masala.util.TestDataUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

import static com.simple_software.swim_masala.util.TestDataUtils.createGossip;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class FailureDetectorTest {

    @Mock
    private NodeClock nodeClock;

    private GossipCollector gossipCollector;
    private FailureDetector failureDetector;
    private PeerTracker peerTracker;

    @Mock
    private Pinger pinger;
    private Peer aPeer;
    private Instant now;
    private Peer selfAsPeer;
    private IdentityPeerService identityPeerService;

    @Before
    public void setUp() {
        now = Instant.now();
        aPeer = TestDataUtils.peer("Argon", now);
        selfAsPeer = TestDataUtils.selfAsPeer(Instant.now());
        identityPeerService = new IdentityPeerService(selfAsPeer);
        peerTracker = new PeerTracker(identityPeerService, Duration.ofSeconds(0), peer -> {
        }, p -> {
        });
        peerTracker.add(aPeer, Status.UNDEFINED);
        when(nodeClock.currentInstant()).thenReturn(now);
        gossipCollector = new GossipCollector(2, new NumberGenerator(), nodeClock, peer -> peerTracker.addToDeadList(peer), identityPeerService);
        failureDetector = new FailureDetector(
                pinger,
                peerTracker,
                gossipCollector,
                identityPeerService
        );
    }

    @Test
    public void ifPingUnSuccessful_AndIndirectPingAlsoUnsuccessful_thenReturnFalse() {
        when(pinger.ackRequest(Mockito.any(), Mockito.any())).thenReturn(false);
        when(pinger.indirectPing(Mockito.any(), Mockito.any())).thenReturn(false);
        failureDetector.detectFailure();

        assertThat(peerTracker.peers().size(), equalTo(1));
        assertThat(peerTracker.peers(), Matchers.containsInAnyOrder(aPeer));
        assertThat(peerTracker.status(aPeer), equalTo(Status.SUSPICIOUS));

        Optional<Gossip> optionalGossip = getGossip();
        optionalGossip.ifPresent(gossip -> assertThat(gossip, equalTo(createGossip(aPeer, selfAsPeer, Status.SUSPICIOUS))));

        verify(pinger).indirectPing(ArgumentMatchers.any(), ArgumentMatchers.any());
        verify(pinger).ackRequest(ArgumentMatchers.any(), ArgumentMatchers.any());
    }

    @Test
    public void ifPingSuccessful_forANewNode_thenAddToPeerTracker() {
        when(pinger.ackRequest(Mockito.any(), Mockito.any())).thenReturn(true);

        failureDetector.detectFailure();

        assertThat(peerTracker.peers().size(), equalTo(1));
        assertThat(peerTracker.peers(), Matchers.containsInAnyOrder(aPeer));
        assertThat(peerTracker.status(aPeer), equalTo(Status.ALIVE));

        Optional<Gossip> optionalGossip = getGossip();
        optionalGossip.ifPresent(gossip -> assertThat(gossip, equalTo(createGossip(aPeer, selfAsPeer, Status.ALIVE))));

        verify(pinger, never()).indirectPing(ArgumentMatchers.any(), ArgumentMatchers.any());

    }

    @Test
    public void ifPingUnSuccessful_butIndirectPingSuccessful_forANewNode_thenAddToPeerTracker() {
        when(pinger.ackRequest(Mockito.any(), Mockito.any())).thenReturn(false);
        when(pinger.indirectPing(Mockito.any(), Mockito.any())).thenReturn(true);

        failureDetector.detectFailure();

        assertThat(peerTracker.peers().size(), equalTo(1));
        assertThat(peerTracker.peers(), Matchers.containsInAnyOrder(aPeer));
        assertThat(peerTracker.status(aPeer), equalTo(Status.ALIVE));

        Optional<Gossip> optionalGossip = getGossip();
        optionalGossip.ifPresent(gossip -> assertThat(gossip, equalTo(createGossip(aPeer, selfAsPeer, Status.ALIVE))));

        verify(pinger).indirectPing(ArgumentMatchers.any(), ArgumentMatchers.any());
        verify(pinger).ackRequest(ArgumentMatchers.any(), ArgumentMatchers.any());

    }

    @Test
    public void ifPeerIsNowSuspicious_thenGossip_andAddToPeerTracker() {
        when(pinger.ackRequest(Mockito.any(), Mockito.any())).thenReturn(false);
        when(pinger.indirectPing(Mockito.any(), Mockito.any())).thenReturn(false);
        peerTracker.add(aPeer, Status.ALIVE);
        failureDetector = new FailureDetector(
                pinger,
                peerTracker,
                gossipCollector, identityPeerService
        );

        failureDetector.detectFailure();

        assertThat(peerTracker.peers().size(), equalTo(1));
        assertThat(peerTracker.peers(), Matchers.containsInAnyOrder(aPeer));
        assertThat(peerTracker.status(aPeer), equalTo(Status.SUSPICIOUS));

        Optional<Gossip> optionalGossip = getGossip();
        optionalGossip.ifPresent(gossip -> assertThat(gossip, equalTo(createGossip(aPeer, selfAsPeer, Status.SUSPICIOUS))));

        verify(pinger).indirectPing(ArgumentMatchers.any(), ArgumentMatchers.any());
        verify(pinger).ackRequest(ArgumentMatchers.any(), ArgumentMatchers.any());

    }

    @Test
    public void gossipOnlyWhenPeerStatusHasChanged() {
        peerTracker.add(aPeer, Status.ALIVE);
        when(pinger.ackRequest(Mockito.any(), Mockito.any())).thenReturn(Boolean.TRUE);
        failureDetector = new FailureDetector(
                pinger,
                peerTracker,
                gossipCollector, identityPeerService
        );

        failureDetector.detectFailure();

        assertThat(peerTracker.peers().size(), equalTo(1));
        assertThat(peerTracker.peers(), Matchers.containsInAnyOrder(aPeer));
        assertThat(peerTracker.status(aPeer), equalTo(Status.ALIVE));
        assertThat(gossipCollector.latestGossip().isPresent(), equalTo(false));

    }

    private Optional<Gossip> getGossip() {
        assertThat(gossipCollector.size(), equalTo(1));
        Optional<Gossip> optionalGossip = gossipCollector.latestGossip();
        assertThat(optionalGossip.isPresent(), equalTo(true));
        return optionalGossip;
    }

}