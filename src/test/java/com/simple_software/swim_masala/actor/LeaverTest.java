package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.supportactor.Pinger;
import com.simple_software.swim_masala.util.NetworkQueryResolver;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;

import java.time.Instant;
import java.util.concurrent.ExecutorService;

import static com.simple_software.swim_masala.util.TestDataUtils.peer;
import static java.util.Arrays.asList;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class LeaverTest {

    private Leaver leaver;
    private String localHostname;
    private ExecutorService receiverService;
    private Pinger pinger;
    private PeerTracker peerTracker;
    private Peer selfAsPeer;
    private Instant peerIncarnationInstant = Instant.ofEpochSecond(1518527759);

    @Before
    public void setUp() throws Exception {
        receiverService = mock(ExecutorService.class);
        pinger = mock(Pinger.class);
        peerTracker = mock(PeerTracker.class);
        leaver = new Leaver(asList(receiverService), peerTracker, pinger);
        localHostname = new NetworkQueryResolver().getHostname();
        selfAsPeer = peer(localHostname, peerIncarnationInstant);
    }

    @Test
    public void whenLeaving_willShutdownAllExecutionThreads_thenInformSomePeersAboutDeparture(){
        Peer peer1 = peer("host1", peerIncarnationInstant);
        Peer peer2 = peer("host2", peerIncarnationInstant);
        when(peerTracker.getAlivePeers(2)).thenReturn(asList(peer1, peer2));

        leaver.leave(new IdentityPeerService(selfAsPeer));

        InOrder inOrder = inOrder(receiverService, peerTracker, pinger);
        inOrder.verify(receiverService).shutdownNow();
        inOrder.verify(peerTracker).getAlivePeers(2);

        ArgumentCaptor<Peer> peerArgumentCaptor = ArgumentCaptor.forClass(Peer.class);
        ArgumentCaptor<Peer> selfPeerArgumentCaptor = ArgumentCaptor.forClass(Peer.class);
        inOrder.verify(pinger, times(2)).sayByeTo(peerArgumentCaptor.capture(), selfPeerArgumentCaptor.capture());
        assertThat(peerArgumentCaptor.getAllValues(), Matchers.containsInAnyOrder(peer1, peer2));
        assertThat(selfPeerArgumentCaptor.getValue(), Matchers.equalTo(selfAsPeer));
    }

}