package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.supportactor.Pinger;
import com.simple_software.swim_masala.util.NetworkQueryResolver;
import com.simple_software.swim_masala.util.TestDataUtils;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.net.ServerSocket;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutorService;

import static com.simple_software.swim_masala.util.TestDataUtils.selfAsPeer;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.*;

public class PingerTest {


    private static final int NONSENSICAL_PORT = -1;
    private Pinger pinger;
    private NetworkQueryResolver networkQueryResolver;
    private PingReceiver pingReceiver;
    private ServerSocket pingReceiverSocket;
    private Duration pingTimeout = Duration.ofSeconds(5);
    private Duration indirectPingTimeout = Duration.ofSeconds(10);
    private Peer remotePeer;
    private IdentityPeerService identityPeerService;

    @Before
    public void setUp() throws Exception {
        identityPeerService = new IdentityPeerService(TestDataUtils.selfAsPeer(Instant.now()));
        pinger = new Pinger(pingTimeout, indirectPingTimeout);
        networkQueryResolver = new NetworkQueryResolver();

        pingReceiverSocket = networkQueryResolver.getServerSocket();
        PeerTracker peerTracker = mock(PeerTracker.class);
        when(peerTracker.getIdentityPeer()).thenReturn(identityPeerService.identityPeer());
        pingReceiver = new PingReceiver(pingReceiverSocket, mock(ExecutorService.class), peerTracker, mock(GossipCollector.class), new Pinger(pingTimeout, indirectPingTimeout));
        startPingReceiver(pingReceiver, pingReceiverSocket);
        remotePeer = selfAsPeer(Instant.now(), pingReceiverSocket.getLocalPort(), -1);
    }

    @Test
    public void returnTrueIfPeerIsReachable() {
        assertThat(pinger.ackRequest(remotePeer, identityPeerService.identityPeer()), Matchers.equalTo(true));
    }

    @Test
    public void returnFalseIfPeerIsUnReachable() {
        assertThat(pinger.ackRequest(selfAsPeer(Instant.now()), identityPeerService.identityPeer()), Matchers.equalTo(false));
    }

    @Test
    public void indirectPingerWillReturnFalse_ifBothThePeersReturnFalse(){
        Peer peer1 = mock(Peer.class);
        Peer peer2 = mock(Peer.class);
        List<Peer> viaPeers = Arrays.asList(peer1, peer2);

        assertThat(pinger.indirectPing(remotePeer, viaPeers), Matchers.equalTo(false));
    }

    @Test
    public void indirectPingerWillReturnTrue_ifAtLeastOnePeerSaysTargetHostIsAlive(){
        ServerSocket pingReceiverSocketAtPeer1 = networkQueryResolver.getServerSocket();
        Peer peer1 = selfAsPeer(Instant.now(), pingReceiverSocketAtPeer1.getLocalPort(),NONSENSICAL_PORT);
        PeerTracker peerTracker = mock(PeerTracker.class);
        when(peerTracker.getIdentityPeer()).thenReturn(peer1);
        PingReceiver pingReceiverAtPeer1 = new PingReceiver(pingReceiverSocketAtPeer1, mock(ExecutorService.class), peerTracker, mock(GossipCollector.class), new Pinger(pingTimeout, indirectPingTimeout));
        startPingReceiver(pingReceiverAtPeer1, pingReceiverSocketAtPeer1);

        Peer peer2 = mock(Peer.class);
        List<Peer> viaPeers = Arrays.asList(peer1, peer2);

        assertThat(pinger.indirectPing(selfAsPeer(Instant.now(), pingReceiverSocket.getLocalPort(), NONSENSICAL_PORT), viaPeers), Matchers.equalTo(true));
    }

    @Test
    public void returnFalseIfByeMessageCannotBeSent(){
        assertThat(pinger.sayByeTo(selfAsPeer(Instant.now()), selfAsPeer(Instant.now())), Matchers.equalTo(false));
    }

    @Test
    public void returnTrueIfByeMessageCanBeSent(){
        Peer thisPeer = mock(Peer.class);
        when(thisPeer.asString()).thenReturn(selfAsPeer(Instant.now()).asString());
        assertThat(pinger.sayByeTo(remotePeer, thisPeer), Matchers.equalTo(true));
        verify(thisPeer).asString();
    }

    private CompletableFuture<Void> startPingReceiver(PingReceiver pingReceiver, ServerSocket pingReceiverSocket) {
        return CompletableFuture.runAsync(() -> {
            try {
                pingReceiver.processSocket(pingReceiverSocket.accept());
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        });
    }
}