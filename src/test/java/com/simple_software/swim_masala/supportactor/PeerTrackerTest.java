package com.simple_software.swim_masala.supportactor;

import com.simple_software.swim_masala.model.ADD_STATUS;
import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.util.Pair;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.time.Duration;
import java.time.Instant;
import java.util.Optional;
import java.util.stream.Collectors;

import static com.simple_software.swim_masala.util.TestDataUtils.peer;
import static com.simple_software.swim_masala.util.TestDataUtils.selfAsPeer;
import static java.util.Optional.of;
import static org.hamcrest.Matchers.anyOf;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

public class PeerTrackerTest {

    private Instant peerIncarnationInstant = Instant.ofEpochSecond(123423455);
    private final Peer peer1 = peer("Surrey",peerIncarnationInstant);
    private final Peer peer2 = peer("Balbizar",peerIncarnationInstant);
    private final Peer peer3 = peer("Balbizar",peerIncarnationInstant, 200);
    private final Peer peer4 = peer("Lorange",peerIncarnationInstant);
    private final Peer suspiciousPeer = peer("Narnia",peerIncarnationInstant);
    private PeerTracker peerTracker;
    private Peer selfAsPeer;

    @Before
    public void setUp() {
        selfAsPeer = selfAsPeer(peerIncarnationInstant);
        peerTracker = new PeerTracker(new IdentityPeerService(selfAsPeer), Duration.ofSeconds(0), peer -> {
        }, p -> {
        });
    }

    @After
    public void tearDown() {
        assertThat(isInternalStructureInSync(), equalTo(true));
    }

    @Test
    public void returnsCorrectStatusOnEveryAdd() {
        whenPeerAdded(peer1, Status.ALIVE)
                .peerAddStatusIs(ADD_STATUS.ADDED)
                .peerTrackerStatusIs(Status.ALIVE);

        whenPeerAdded(peer2, Status.ALIVE)
                .peerAddStatusIs(ADD_STATUS.ADDED)
                .peerTrackerStatusIs(Status.ALIVE);

        whenPeerAdded(peer2, Status.ALIVE)
                .peerAddStatusIs(ADD_STATUS.NOT_ADDED)
                .peerTrackerStatusIs(Status.ALIVE);

        whenPeerAdded(peer3, Status.ALIVE)
                .peerAddStatusIs(ADD_STATUS.ADDED)
                .peerTrackerStatusIs(Status.ALIVE);

        whenPeerAdded(peer3, Status.SUSPICIOUS)
                .peerAddStatusIs(ADD_STATUS.ADDED)
                .peerTrackerStatusIs(Status.SUSPICIOUS);

        whenPeerAdded(peer3, Status.DEAD)
                .peerAddStatusIs(ADD_STATUS.ADDED)
                .peerTrackerStatusIs(Status.DEAD);

        whenPeerAdded(suspiciousPeer, Status.SUSPICIOUS)
                .peerAddStatusIs(ADD_STATUS.ADDED)
                .peerTrackerStatusIs(Status.SUSPICIOUS);

        whenPeerAdded(suspiciousPeer, Status.ALIVE)
                .peerAddStatusIs(ADD_STATUS.ADDED)
                .peerTrackerStatusIs(Status.ALIVE);

        whenPeerAdded(suspiciousPeer, Status.SUSPICIOUS)
                .peerAddStatusIs(ADD_STATUS.ADDED)
                .peerTrackerStatusIs(Status.SUSPICIOUS);

        whenPeerAdded(suspiciousPeer, Status.SUSPICIOUS)
                .peerAddStatusIs(ADD_STATUS.NOT_ADDED)
                .peerTrackerStatusIs(Status.SUSPICIOUS);
    }

    @Test
    public void pingerGetsHost_InRoundRobinFashion_OnEveryAsk() {
        peerTracker.add(peer2, Status.ALIVE);
        peerTracker.add(peer3, Status.ALIVE);
        peerTracker.add(peer4, Status.ALIVE);

        assertThat(peerTracker.getPingPeer(), equalTo(of(peer2)));
        assertThat(peerTracker.getPingPeer(), equalTo(of(peer3)));
        assertThat(peerTracker.getPingPeer(), equalTo(of(peer4)));
        assertThat(peerTracker.getPingPeer(), anyOf(equalTo(of(peer2)), equalTo(of(peer4)), equalTo(of(peer3))));
    }

    @Test
    public void whenAddingPeer_ignorePeerOfPreviousIncarnation() {
        Instant now = peerIncarnationInstant;
        Instant later = now.plusSeconds(1);

        Peer peerOldIncarnation = peer("Hobbit",now);
        Peer peerNewIncarnation = peer("Hobbit",later);


        peerTracker.add(peerNewIncarnation, Status.ALIVE);
        ADD_STATUS addStatus = peerTracker.add(peerOldIncarnation, Status.ALIVE);
        assertThat(addStatus, equalTo(ADD_STATUS.NOT_ADDED));

        assertThat(peerTracker.getPingPeer(), equalTo(of(peerNewIncarnation)));
        assertThat(peerTracker.peers().size(), equalTo(1));

    }

    @Test
    public void whenAddingPeer_removePreviousIncarnationInstance() {
        Instant now = peerIncarnationInstant;
        Instant later = now.plusSeconds(1);

        Peer peerOldIncarnation = peer("Hobbit",now);
        Peer peerNewIncarnation = peer("Hobbit",later);

        assertThat(peerTracker.add(peerOldIncarnation, Status.ALIVE), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.add(peerNewIncarnation, Status.ALIVE), equalTo(ADD_STATUS.ADDED));

        assertThat(peerTracker.getPingPeer(), equalTo(of(peerNewIncarnation)));
        assertThat(peerTracker.peers().size(), equalTo(1));

    }


    @Test
    public void shouldReturnOnlyLiveHostsForIndirectPing_butShouldExcludeTargetHostEvenIfAlive() {

        peerTracker.add(peer1, Status.SUSPICIOUS);
        peerTracker.add(peer2, Status.DEAD);
        peerTracker.add(peer3, Status.ALIVE);

        assertThat(peerTracker.getIndirectPingPeers(peer3), Matchers.empty());

        Peer peer2New = new IdentityPeerService(peer2).newIncarnation();
        peerTracker.add(peer2New, Status.ALIVE);
        peerTracker.add(peer4, Status.ALIVE);

        assertThat(peerTracker.getIndirectPingPeers(peer3), Matchers.contains(peer2New, peer4));
    }


    @Test
    public void neverReturnSelfAsAResult(){
        peerTracker.add(peer1, Status.SUSPICIOUS);
        peerTracker.add(peer2, Status.DEAD);
        peerTracker.add(peer3, Status.ALIVE);
        peerTracker.add(selfAsPeer, Status.ALIVE);

        assertThat(peerTracker.getAlivePeers(2), Matchers.containsInAnyOrder(peer3));
        assertThat(peerTracker.getPingPeer(), equalTo(of(peer1)));
        assertThat(peerTracker.getIndirectPingPeers(peer1), Matchers.containsInAnyOrder(peer3));

    }

    @Test
    public void mustHoldOnlyTheLatestIncarnationOfPeer(){
        IdentityPeerService identityPeerService = new IdentityPeerService(peer1);
        Peer newIncarnationOfPeer = identityPeerService.newIncarnation();

        assertThat(peerTracker.add(peer1, Status.ALIVE), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.add(newIncarnationOfPeer, Status.ALIVE), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.peers(), Matchers.containsInAnyOrder(newIncarnationOfPeer));
        assertThat(peerTracker.getADeadPeer(), Matchers.equalTo(Optional.empty()));

    }

    @Test
    public void mustHoldOnlyTheLatestIncarnationOfPeer_whenThePreviousIncarnationWasDead(){
        IdentityPeerService identityPeerService = new IdentityPeerService(peer1);
        Peer newIncarnationOfPeer = identityPeerService.newIncarnation();

        assertThat(peerTracker.add(peer1, Status.DEAD), equalTo(ADD_STATUS.ADDED));

        assertThat(peerTracker.add(newIncarnationOfPeer, Status.ALIVE), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.peers(), Matchers.containsInAnyOrder(newIncarnationOfPeer));
        assertThat(peerTracker.getADeadPeer(), Matchers.equalTo(Optional.empty()));

    }

    @Test
    public void alivePeersWillNotBeAdded_whenALaterVersionOfThePeerHasBeenDeclaredDead(){
        assertThat(peerTracker.add(peer1, Status.ALIVE), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.add(peer1, Status.DEAD), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.add(peer1, Status.ALIVE), equalTo(ADD_STATUS.NOT_ADDED));

        IdentityPeerService identityPeerService = new IdentityPeerService(peer1);
        Peer newPeer1 = identityPeerService.newIncarnation();

        assertThat(peerTracker.add(newPeer1, Status.ALIVE), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.add(peer1, Status.SUSPICIOUS), equalTo(ADD_STATUS.NOT_ADDED));
        assertThat(peerTracker.add(peer1, Status.DEAD), equalTo(ADD_STATUS.NOT_ADDED));

        Peer newPeer2 = identityPeerService.newIncarnation();
        assertThat(peerTracker.add(newPeer1, Status.DEAD), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.add(newPeer2, Status.DEAD), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.add(peer1, Status.ALIVE), equalTo(ADD_STATUS.NOT_ADDED));
        assertThat(peerTracker.add(newPeer2, Status.ALIVE), equalTo(ADD_STATUS.NOT_ADDED));

        Peer newPeer3 = identityPeerService.newIncarnation();
        assertThat(peerTracker.add(newPeer3, Status.ALIVE), equalTo(ADD_STATUS.ADDED));
    }

    @Test
    public void ifThereAreMultipleIncarnationsOfDeadPeer_thenOnlyOneIncarnationIsMaintained() {
        assertThat(peerTracker.add(peer1, Status.DEAD), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.add(peer1, Status.DEAD), equalTo(ADD_STATUS.NOT_ADDED));
        assertThat(peerTracker.getAllDeadPeers(), Matchers.contains(peer1));

        IdentityPeerService identityPeerService = new IdentityPeerService(peer1);
        Peer newPeer1 = identityPeerService.newIncarnation();
        assertThat(peerTracker.add(newPeer1, Status.DEAD), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.getAllDeadPeers(), Matchers.contains(newPeer1));

        Peer newPeer2 = identityPeerService.newIncarnation();
        assertThat(peerTracker.add(newPeer1, Status.ALIVE), equalTo(ADD_STATUS.NOT_ADDED));
        assertThat(peerTracker.getAllDeadPeers(), Matchers.contains(newPeer1));
        assertThat(peerTracker.add(newPeer2, Status.ALIVE), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.getAllDeadPeers(), Matchers.empty());
        assertThat(peerTracker.add(newPeer2, Status.DEAD), equalTo(ADD_STATUS.ADDED));
        assertThat(peerTracker.getAllDeadPeers(), Matchers.contains(newPeer2));


    }

    private PeerAddStatus whenPeerAdded(Peer peer, Status status) {
        return peerAddStatus -> st -> {
            assertThat(peerTracker.add(peer, status), equalTo(peerAddStatus));
            assertThat(peerTracker.status(peer), equalTo(st));
        };
    }

    private boolean isInternalStructureInSync() {
        return peerTracker.peers().size() == peerTracker.statusMap().size()
                && peerTracker.peers().containsAll(peerTracker.statusMap().stream().map(Pair::getKey).collect(Collectors.toList()));
    }

    interface PeerAddStatus {
        PeerTrackerStatus peerAddStatusIs(ADD_STATUS peerAddStatus);
    }

    interface PeerTrackerStatus {
        void peerTrackerStatusIs(Status status);
    }

}