package com.simple_software.swim_masala.supportactor;

import com.simple_software.swim_masala.model.*;
import com.simple_software.swim_masala.util.Pair;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Consumer;

import static com.simple_software.swim_masala.util.TestDataUtils.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.mockito.Mockito.when;

public class GossipCollectorTest {

    private GossipCollector gossipCollector;
    private Instant now = Instant.now();
    private Instant later = now.plusSeconds(1);
    private NodeClock nodeClock = Mockito.mock(NodeClock.class);
    private NumberGenerator numberGenerator;
    private PeerTracker peerTracker;
    private Consumer<Peer> peerTrackerUpdater = peer -> peerTracker.addToDeadList(peer);
    private Peer selfAsPeer;
    private IdentityPeerService identityPeerService;

    @Before
    public void setUp() {
        selfAsPeer = selfAsPeer(now);
        identityPeerService = new IdentityPeerService(selfAsPeer);
        peerTracker = new PeerTracker(identityPeerService, Duration.ofSeconds(0), peer -> {
        }, p -> {
        });
        when(nodeClock.currentInstant()).thenReturn(now);
        numberGenerator = new NumberGenerator();
        gossipCollector = new GossipCollector(20, numberGenerator, nodeClock, peerTrackerUpdater, identityPeerService);
    }

    @Test
    public void returnLatestGossip() {
        assertThat(gossipCollector.gossip(createGossip(peer("Medusa", now), selfAsPeer(now), Status.ALIVE, 2), 5), Matchers.equalTo(ADD_STATUS.ADDED));
        assertThat(gossipCollector.gossip(createGossip(peer("Athena", now), selfAsPeer(now), Status.ALIVE, 1), 4), Matchers.equalTo(ADD_STATUS.ADDED));

        Optional<Gossip> latestGossipOptional = gossipCollector.latestGossip();
        assertThat(latestGossipOptional.isPresent(), Matchers.is(true));
        latestGossipOptional.ifPresent(latestGossip -> assertThat(latestGossip.getAboutPeer(), equalTo(peer("Athena", now))));

        List<Gossip> allGossips = gossipCollector.allTheLatestGossips();
        assertThat(allGossips.size(), equalTo(2));
        assertThat(allGossips.get(0).getAboutPeer(), equalTo(peer("Medusa", now)));
        assertThat(allGossips.get(1).getAboutPeer(), equalTo(peer("Athena", now)));

        latestGossipOptional = gossipCollector.latestGossip();
        assertThat(latestGossipOptional.isPresent(), Matchers.is(true));
        latestGossipOptional.ifPresent(latestGossip -> assertThat(latestGossip.getAboutPeer(), equalTo(peer("Medusa", now))));

        allGossips = gossipCollector.allTheLatestGossips();
        assertThat(allGossips.size(), equalTo(2));
        assertThat(allGossips.get(0).getAboutPeer(), equalTo(peer("Athena", now)));
        assertThat(allGossips.get(1).getAboutPeer(), equalTo(peer("Medusa", now)));

    }

    @Test
    public void whenGossipped_thenIncrementAgeOfGossip() {
        assertThat(gossipCollector.gossip(createGossip(peer("Medusa", now), selfAsPeer(now), Status.ALIVE, 1), 5), Matchers.equalTo(ADD_STATUS.ADDED));
        assertThat(gossipCollector.gossip(createGossip(peer("Athena", now), selfAsPeer(now), Status.ALIVE, 2), 4), Matchers.equalTo(ADD_STATUS.ADDED));

        Optional<Gossip> latestGossipOptional = gossipCollector.latestGossip();
        assertThat(latestGossipOptional.isPresent(), Matchers.is(true));
        latestGossipOptional.ifPresent(latestGossip -> assertThat(latestGossip.getAboutPeer(), equalTo(peer("Athena", now))));

        latestGossipOptional = gossipCollector.latestGossip();
        assertThat(latestGossipOptional.isPresent(), Matchers.is(true));
        latestGossipOptional.ifPresent(latestGossip -> assertThat(latestGossip.getAboutPeer(), equalTo(peer("Athena", now))));

        latestGossipOptional = gossipCollector.latestGossip();
        assertThat(latestGossipOptional.isPresent(), Matchers.is(true));
        latestGossipOptional.ifPresent(latestGossip -> assertThat(latestGossip.getAboutPeer(), equalTo(peer("Medusa", now))));

    }


    @Test
    public void staleGossipIsIgnored() {
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 1), 40), Matchers.equalTo(ADD_STATUS.NOT_ADDED));

        Optional<Gossip> latestGossipOptional = gossipCollector.latestGossip();

        assertThat(latestGossipOptional.isPresent(), Matchers.is(false));
    }

    @Test
    public void canReAddOnceStaleGossip_ifTheHostPublishesGossipWithAHigherSequenceNumber() {
        gossipCollector = new GossipCollector(1, numberGenerator, nodeClock, peerTrackerUpdater, identityPeerService);

        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 25)), Matchers.equalTo(ADD_STATUS.ADDED));
        Optional<Gossip> latestGossipOptional = gossipCollector.latestGossip();

        assertThat(latestGossipOptional.isPresent(), Matchers.is(true));
        latestGossipOptional.ifPresent(gossip -> assertThat(gossip.getAboutPeer(), equalTo(peer("Zeus", now))));
        assertThat(gossipCollector.latestGossip().isPresent(), Matchers.is(false));

        //Re-add
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 26)), Matchers.equalTo(ADD_STATUS.ADDED));

        latestGossipOptional = gossipCollector.latestGossip();
        assertThat(latestGossipOptional.isPresent(), Matchers.is(true));
        latestGossipOptional.ifPresent(gossip -> assertThat(gossip.getAboutPeer(), equalTo(peer("Zeus", now))));
        assertThat(gossipCollector.latestGossip().isPresent(), Matchers.is(false));
    }

    @Test
    public void canReAddOnceStaleGossip_ifTheHostPublishesGossip_evenIfWithALowerSequenceNumber() {
        gossipCollector = new GossipCollector(1, numberGenerator, nodeClock, peerTrackerUpdater, identityPeerService);

        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 25)), Matchers.equalTo(ADD_STATUS.ADDED));
        Optional<Gossip> latestGossipOptional = gossipCollector.latestGossip();

        assertThat(latestGossipOptional.isPresent(), Matchers.is(true));
        latestGossipOptional.ifPresent(gossip -> assertThat(gossip.getAboutPeer(), equalTo(peer("Zeus", now))));
        assertThat(gossipCollector.latestGossip().isPresent(), Matchers.is(false));

        //Re-add
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 1)), Matchers.equalTo(ADD_STATUS.ADDED));

        latestGossipOptional = gossipCollector.latestGossip();
        assertThat(latestGossipOptional.isPresent(), Matchers.is(true));
        latestGossipOptional.ifPresent(gossip -> assertThat(gossip.getAboutPeer(), equalTo(peer("Zeus", now))));
        assertThat(gossipCollector.latestGossip().isPresent(), Matchers.is(false));
    }

    @Test
    public void latestSequenceOfGossipIsAlwaysPreferred() {
        gossipCollector = new GossipCollector(1, numberGenerator, nodeClock, peerTrackerUpdater, identityPeerService);
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 3)), Matchers.equalTo(ADD_STATUS.ADDED));
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 2)), Matchers.equalTo(ADD_STATUS.NOT_ADDED));
        assertThat(gossipCollector.gossip(createGossip(peer("Hercules", now), selfAsPeer(now), Status.ALIVE, 1)), Matchers.equalTo(ADD_STATUS.ADDED));

        assertThat(gossipCollector.size(), equalTo(2));

        Optional<Gossip> gossipOptional = gossipCollector.latestGossip();
        assertThat(gossipOptional.isPresent(), equalTo(true));
        gossipOptional.ifPresent(gossip -> {
            assertThat(gossip.getAboutPeer(), equalTo(peer("Zeus", now)));
            assertThat(gossip.getSequenceNumber(), equalTo(3L));
        });

        gossipOptional = gossipCollector.latestGossip();
        assertThat(gossipOptional.isPresent(), equalTo(true));
        gossipOptional.ifPresent(gossip -> {
            assertThat(gossip.getAboutPeer(), equalTo(peer("Hercules", now)));
            assertThat(gossip.getSequenceNumber(), equalTo(1L));
        });

        assertThat(gossipCollector.latestGossip().isPresent(), equalTo(false));
    }

    @Test
    public void latestSequenceOfGossipRemovesPreviousSequence() {
        gossipCollector = new GossipCollector(1, numberGenerator, nodeClock, peerTrackerUpdater, identityPeerService);
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 2)), Matchers.equalTo(ADD_STATUS.ADDED));
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 3)), Matchers.equalTo(ADD_STATUS.ADDED));
        assertThat(gossipCollector.gossip(createGossip(peer("Hercules", now), selfAsPeer(now), Status.ALIVE, 1)), Matchers.equalTo(ADD_STATUS.ADDED));

        assertThat(gossipCollector.size(), equalTo(2));

        Optional<Gossip> gossipOptional = gossipCollector.latestGossip();
        assertThat(gossipOptional.isPresent(), equalTo(true));
        gossipOptional.ifPresent(gossip -> {
            assertThat(gossip.getAboutPeer(), equalTo(peer("Zeus", now)));
            assertThat(gossip.getSequenceNumber(), equalTo(3L));
        });

        gossipOptional = gossipCollector.latestGossip();
        assertThat(gossipOptional.isPresent(), equalTo(true));
        gossipOptional.ifPresent(gossip -> {
            assertThat(gossip.getAboutPeer(), equalTo(peer("Hercules", now)));
            assertThat(gossip.getSequenceNumber(), equalTo(1L));
        });

        assertThat(gossipCollector.latestGossip().isPresent(), equalTo(false));
    }

    @Test
    public void whenGossipOfAnNewSequencePeerIsReceived_thenOldGossipOfThatPeerIsRemoved() {
        gossipCollector = new GossipCollector(1, numberGenerator, nodeClock, peerTrackerUpdater, identityPeerService);
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 10)), Matchers.equalTo(ADD_STATUS.ADDED));
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", later), selfAsPeer(later), Status.ALIVE, 1)), Matchers.equalTo(ADD_STATUS.ADDED));
        assertThat(gossipCollector.size(), equalTo(1));

        Optional<Gossip> gossipOptional = gossipCollector.latestGossip();
        assertThat(gossipOptional.isPresent(), equalTo(true));
        gossipOptional.ifPresent(gossip -> {
            assertThat(gossip.getAboutPeer(), equalTo(peer("Zeus", later)));
            assertThat(gossip.getSequenceNumber(), equalTo(1L));
        });
    }

    @Test
    public void whenGossipOfAnOldSequencePeerIsReceived_thenItIsIgnored() {
        gossipCollector = new GossipCollector(1, numberGenerator, nodeClock, peerTrackerUpdater, identityPeerService);
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", later), selfAsPeer(later), Status.ALIVE, 1)), Matchers.equalTo(ADD_STATUS.ADDED));
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 10)), Matchers.equalTo(ADD_STATUS.NOT_ADDED));
        assertThat(gossipCollector.size(), equalTo(1));

        Optional<Gossip> gossipOptional = gossipCollector.latestGossip();
        assertThat(gossipOptional.isPresent(), equalTo(true));
        gossipOptional.ifPresent(gossip -> {
            assertThat(gossip.getAboutPeer(), equalTo(peer("Zeus", later)));
            assertThat(gossip.getSequenceNumber(), equalTo(1L));
        });
    }

    @Test
    public void ifStaleGossipIsAboutSuspiciousHost_thenRemoveDeadHostFromList() {
        Peer zeusPeer = peer("Zeus", now);
        gossipCollector = new GossipCollector(1, numberGenerator, nodeClock, peerTrackerUpdater, identityPeerService);
        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.SUSPICIOUS, 10)), Matchers.equalTo(ADD_STATUS.ADDED));
        assertThat(gossipCollector.size(), equalTo(1));

        Optional<Gossip> gossipOptional = gossipCollector.latestGossip();

        assertThat(gossipOptional.isPresent(), equalTo(true));
        gossipOptional.ifPresent(gossip -> {
            assertThat(gossip.getAboutPeer(), equalTo(zeusPeer));
            assertThat(gossip.getSequenceNumber(), equalTo(10L));
            assertThat(gossip.getStatus(), equalTo(Status.SUSPICIOUS));
        });
        assertThat(gossipCollector.size(), equalTo(1));

        gossipOptional = gossipCollector.latestGossip();

        assertThat(gossipOptional.isPresent(), equalTo(true));
        gossipOptional.ifPresent(gossip -> {
            assertThat(gossip.getAboutPeer(), equalTo(zeusPeer));
            assertThat(gossip.getSequenceNumber(), equalTo(1L));
            assertThat(gossip.getStatus(), equalTo(Status.DEAD));
        });
        assertThat(gossipCollector.size(), equalTo(0));

        assertThat(peerTracker.statusMap().size(), equalTo(0));
    }

    @Test
    public void gossipCollectorRespectsIncarnationOfNewGossips() {
        gossipCollector = new GossipCollector(1, numberGenerator, nodeClock, peerTrackerUpdater, identityPeerService);

        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", now), selfAsPeer(now), Status.ALIVE, 10)), Matchers.equalTo(ADD_STATUS.ADDED));

        assertThat(gossipCollector.gossip(createGossip(peer("Zeus", later), selfAsPeer(later), Status.ALIVE, 1)), Matchers.equalTo(ADD_STATUS.ADDED));

        Peer zeusNew = peer("Zeus", 2, later);
        Gossip latestIncarnationOfNewPeerInstance = createGossip(zeusNew, peer("host1", later), Status.DEAD, 1);
        assertThat(gossipCollector.gossip(latestIncarnationOfNewPeerInstance), Matchers.equalTo(ADD_STATUS.ADDED));

        assertThat(gossipCollector.size(), equalTo(2));

        Map<String, Pair<Peer, Map<String, Gossip>>> gossipsMap = gossipCollector.getGossipsMap();
        assertThat(gossipsMap.size(), Matchers.equalTo(1));
        assertThat(gossipsMap.keySet(), Matchers.containsInAnyOrder("Zeus:100"));

        Pair<Peer, Map<String, Gossip>> statusOfLatestGossip = gossipsMap.get("Zeus:100");
        assertThat(statusOfLatestGossip.getKey(), Matchers.equalTo(zeusNew));
        assertThat(statusOfLatestGossip.getValue().keySet(), Matchers.containsInAnyOrder("host1:100", hostname() + ":100"));
        assertThat(statusOfLatestGossip.getValue().values().size(), Matchers.equalTo(2));

        List<Gossip> allTheLatestGossips = gossipCollector.allTheLatestGossips();
        assertThat(allTheLatestGossips.size(), equalTo(2));

    }


}