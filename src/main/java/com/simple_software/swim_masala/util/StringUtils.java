package com.simple_software.swim_masala.util;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class StringUtils {
    public static String asString(Collection<String> aCollection, String delimiter) {
        return aCollection.stream().collect(Collectors.joining(delimiter)).trim();
    }

    static List<String> splitStringOnNewLine(String content, int maxSizeOfSplitInBytes) {
        byte[] originalBytes = content.getBytes(Charset.forName("UTF-8"));
        if (originalBytes.length > maxSizeOfSplitInBytes) {
            mustContainNewLIne(content);
            List<String> splitContent = new ArrayList<>();
            ByteBuffer byteBuffer = ByteBuffer.allocate(maxSizeOfSplitInBytes);
            for (byte originalByte : originalBytes) {
                byteBuffer.put(originalByte);
                markInBufferIfNewLine(byteBuffer, originalByte);
                if (readTillBufferCapacity(byteBuffer)) {
                    byteBuffer.reset();
                    String result = readByteBuffer(byteBuffer);
                    splitContent.add(result);
                }
            }
            String remaining = readByteBuffer(byteBuffer);
            if (!remaining.trim().isEmpty()) {
                splitContent.add(remaining);
            }
            return splitContent;
        } else {
            return Collections.singletonList(content);
        }
    }

    private static void mustContainNewLIne(String content) {
        if (!content.contains("\n")) {
            throw new RuntimeException("String must contain one or more new line character");
        }
    }

    private static boolean readTillBufferCapacity(ByteBuffer byteBuffer) {
        return byteBuffer.position() == byteBuffer.capacity();
    }

    private static void markInBufferIfNewLine(ByteBuffer byteBuffer, byte originalByte) {
        if (originalByte == 10) {
            byteBuffer.mark();
        }
    }

    private static String readByteBuffer(ByteBuffer byteBuffer) {
        byte[] bytes = new byte[byteBuffer.position()];
        byteBuffer.flip();
        byteBuffer.get(bytes);
        byteBuffer.limit(byteBuffer.capacity());
        byteBuffer.compact();
        return new String(bytes);

    }
}
