package com.simple_software.swim_masala.util;

import java.io.IOException;
import java.net.*;

public class NetworkQueryResolver {
    public  String getHostname(){
        try {
            return InetAddress.getLocalHost().getHostName();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    public InetAddress getLocalAddress() {
        try {
            return InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    public InetAddress getByName(String hostname) {
        try {
            return InetAddress.getByName(hostname);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }

    public DatagramSocket getDatagramSocket() {
        try {
            DatagramSocket datagramSocket = new DatagramSocket();
            datagramSocket.setReuseAddress(true);
            return datagramSocket;
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }

    public ServerSocket getServerSocket() {
        try {
            return new ServerSocket(0);
        } catch (java.io.IOException e) {
            throw new RuntimeException(e);
        }
    }

    public ServerSocket getServerSocket(int portNumber) {
        try {
            ServerSocket serverSocket = new ServerSocket(portNumber);
            serverSocket.setReuseAddress(true);
            return serverSocket;
        } catch (java.io.IOException e) {
            throw new RuntimeException(e);
        }
    }

    public Socket getSocket(String hostname, int port){
        try {
            Socket socket = new Socket(hostname, port);
            socket.setReuseAddress(true);
            return socket;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
