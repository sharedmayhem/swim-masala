package com.simple_software.swim_masala.util;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Note that the methods in this class should not close the streams/sockets it receives to enable request/response
 * type of interaction between a client and server
 */
public class TCPCommunicator {

    public static final String DELIMITER = "@";

    public List<String> readLines(InputStream inputStream) {
        List<String> result = new ArrayList<>();
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                result.addAll(Arrays.asList(line.split(DELIMITER)));
            }
            return result;
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public String readLine(InputStream inputStream) {
        List<String> lines = readLines(inputStream);
        return lines.size() > 0 ? lines.get(0) : "";
    }


    public void writeLines(List<String> content, Socket socket) {
        writeLine(StringUtils.asString(content, DELIMITER), socket);

    }

    public void writeLine(String content, Socket socket) {
        BufferedWriter bufferedWriter = null;
        try {
            bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            bufferedWriter.write(content);
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (bufferedWriter != null) bufferedWriter.flush();
                socket.shutdownOutput();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
