package com.simple_software.swim_masala.util;


import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.util.List;

public class UDPCommunicator {
    private final NetworkQueryResolver networkQueryResolver;
    private int datagramPacketContentSizeLimit;

    public UDPCommunicator(int datagramPacketContentSizeLimit) {
        this(datagramPacketContentSizeLimit, new NetworkQueryResolver());
    }

    UDPCommunicator(int datagramPacketContentSizeLimit, NetworkQueryResolver networkQueryResolver) {
        this.datagramPacketContentSizeLimit = datagramPacketContentSizeLimit;
        this.networkQueryResolver = networkQueryResolver;
    }

    public void send(String targetHostName, int port, String content) {
        List<String> safelySizedContent = StringUtils.splitStringOnNewLine(content, datagramPacketContentSizeLimit);
        try (DatagramSocket datagramSocket = networkQueryResolver.getDatagramSocket()) {
            safelySizedContent.forEach(safeContent -> {
                try {
                    datagramSocket.send(packet(safeContent, networkQueryResolver.getByName(targetHostName), port));
                } catch (IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

    private DatagramPacket packet(String content, InetAddress targetAddress, int port) {
        byte[] bytes = content.getBytes(Charset.forName("UTF-8"));
        return new DatagramPacket(bytes, bytes.length, targetAddress, port);
    }


}
