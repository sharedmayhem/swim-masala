package com.simple_software.swim_masala.supportactor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.util.NetworkQueryResolver;
import com.simple_software.swim_masala.util.TCPCommunicator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.Socket;
import java.time.Duration;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Pinger {

    private static final Logger LOGGER = LoggerFactory.getLogger("SwallowedExceptions");
    private final TCPCommunicator tcpCommunicator;
    private final NetworkQueryResolver networkQueryResolver;

    private final Duration pingTimeout;
    private final Duration indirectPingTimeout;

    public Pinger(Duration pingTimeout, Duration indirectPingTimeout) {
        this(new TCPCommunicator(), new NetworkQueryResolver(), pingTimeout, indirectPingTimeout);
    }

    private Pinger(TCPCommunicator tcpCommunicator, NetworkQueryResolver networkQueryResolver, Duration pingTimeout, Duration indirectPingTimeout) {
        this.tcpCommunicator = tcpCommunicator;
        this.networkQueryResolver = networkQueryResolver;
        this.pingTimeout = pingTimeout;
        this.indirectPingTimeout = indirectPingTimeout;
    }

    public boolean ackRequest(Peer peer, Peer fromPeer) {
        boolean isPeerPingable = ping(peer, "ack-req:" + fromPeer.asString(), pingTimeout, (op) -> op.contains(Status.ALIVE.toString()));
        if (!isPeerPingable) {
            LOGGER.error("Ping of peer {} failed!", peer.logFriendly());
        }
        return isPeerPingable;

    }

    public boolean indirectPing(Peer peer, List<Peer> viaPeers) {
        boolean isPeerPingable = viaPeers.stream()
                .parallel()
                .anyMatch(viaPeer -> ping(viaPeer, "ackRequest-req:" + peer.asString(), indirectPingTimeout, (op) -> op.contains(Status.ALIVE.toString())));
        if (!isPeerPingable) {
            LOGGER.error("Indirect ping of peer {} via peers {} failed!", peer.logFriendly(), viaPeers.stream().map(Peer::logFriendly).collect(Collectors.joining(",")));
        }
        return isPeerPingable;
    }

    public boolean sayByeTo(Peer remotePeer, Peer thisPeer) {
        return ping(remotePeer, "bye-bye:" + thisPeer.asString(), pingTimeout, result -> result.equalsIgnoreCase("ok"));
    }

    private boolean ping(Peer peer, String message, Duration pingTimeout, Function<String, Boolean> resultVerifier) {
        try (Socket socket = networkQueryResolver.getSocket(peer.getHostname(), peer.getPingReceiverPort())) {
            socket.setSoTimeout(Long.valueOf(pingTimeout.toMillis()).intValue());
            tcpCommunicator.writeLine(message, socket);
            String output = tcpCommunicator.readLine(socket.getInputStream());
            return resultVerifier.apply(output);
        } catch (Exception e) {
            LOGGER.error("Ping of peer failed {} because: {}", peer.logFriendly(), e.getMessage());
            return false;
        }
    }
}
