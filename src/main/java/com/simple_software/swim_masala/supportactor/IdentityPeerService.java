package com.simple_software.swim_masala.supportactor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.PeerBuilder;

/**
 * Is responsible for maintaining the mutable identity of this peer instance.
 * The peer is mutated by the gossip receiver in certain cases i.e., a new incarnation number of the peer is generated
 */
public class IdentityPeerService {
    private Peer identityPeer;

    public IdentityPeerService(Peer identityPeer) {
        this.identityPeer = identityPeer;
    }

    public synchronized Peer identityPeer() {
        return identityPeer;
    }

    public synchronized Peer newIncarnation() {
        int incarnationNumber = identityPeer.getIncarnationNumber();
        identityPeer = new PeerBuilder()
                .setIncarnationNumber(++incarnationNumber)
                .setHostname(identityPeer.getHostname())
                .setIncarnationInstant(identityPeer.getIncarnationInstant())
                .setType(identityPeer.getType())
                .setGossipReceiverPort(identityPeer.getGossipReceiverPort())
                .setPingReceiverPort(identityPeer.getPingReceiverPort())
                .setName(identityPeer.getName())
                .setMetadata(identityPeer.getMetadata())
                .createPeer();
        return identityPeer;
    }
}
