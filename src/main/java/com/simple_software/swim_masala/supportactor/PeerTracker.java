package com.simple_software.swim_masala.supportactor;

import com.simple_software.swim_masala.model.ADD_STATUS;
import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.*;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

/**
 * This class collects all peers. This is shared with multiple threads and hence heavily synchronized.
 * This ensures only the latest incarnation of a peer is stored.
 */
public class PeerTracker {

    private static final Logger LOGGER = LoggerFactory.getLogger(PeerTracker.class);
    private final Duration deadPeerWaitDuration;
    private final Consumer<Peer> onDeadPeer;
    private Consumer<Peer> onAlivePeer;
    private Map<String, Pair<Peer, Status>> peersVsStatus = new LinkedHashMap<>();
    private List<Peer> peers = new ArrayList<>();
    private PeerIterator peerIterator = new PeerIterator();
    private IdentityPeerService identityPeerService;
    private DelayQueue<DelayedPeer> peerGraveyard = new DelayQueue<>();

    public PeerTracker(IdentityPeerService identityPeerService, Duration deadPeerWaitDuration, Consumer<Peer> onDeadPeer, Consumer<Peer> onAlivePeer) {
        this.identityPeerService = identityPeerService;
        this.deadPeerWaitDuration = deadPeerWaitDuration;
        this.onDeadPeer = onDeadPeer;
        this.onAlivePeer = onAlivePeer;
    }

    public synchronized Peer getIdentityPeer() {
        return identityPeerService.identityPeer();
    }

    public synchronized Optional<Peer> getPingPeer() {
        return peerIterator.getNextPingPeer();
    }

    public synchronized Collection<Peer> getGossipPeers() {
        Collection<Peer> gossipPeers = new HashSet<>();
        peerIterator.getNextGossipPeer().ifPresent(gossipPeers::add);
        peerIterator.getNextGossipPeer().ifPresent(gossipPeers::add);
        return gossipPeers;
    }

    public synchronized ADD_STATUS add(Peer peer, Status newStatus) {
        if (isTheIdentityPeer(peer)
                || isADeadPeerBeingReAdded(peer)
                || isOldIncarnation(peer)
                || ifPeerExists(peer, newStatus)) {
            return ADD_STATUS.NOT_ADDED;
        }

        if (newStatus == Status.DEAD) {
            addToDeadList(peer);
            onDeadPeer.accept(peer);
            return ADD_STATUS.ADDED;
        }

        peerGraveyard.removeIf(delayedPeer -> delayedPeer.peer.getHostnameKey().equalsIgnoreCase(peer.getHostnameKey()));
        peers.add(peer);

        Pair<Peer, Status> oldPeerStatusPair = peersVsStatus.get(peer.getHostnameKey());
        if (oldPeerStatusPair == null) {
            peersVsStatus.put(peer.getHostnameKey(), new Pair<>(peer, newStatus));
            return newPeerAdded(peer);
        } else {
            peers.remove(oldPeerStatusPair.getKey());
            peersVsStatus.put(peer.getHostnameKey(), new Pair<>(peer, newStatus));
            return newPeerAdded(peer);
        }


    }

    private ADD_STATUS newPeerAdded(Peer peer) {
        onAlivePeer.accept(peer);
        return ADD_STATUS.ADDED;
    }

    public synchronized Optional<Peer> getADeadPeer() {
        DelayedPeer delayedPeer = peerGraveyard.poll();
        if (delayedPeer != null) {
            return Optional.of(delayedPeer.peer);
        }
        return Optional.empty();
    }

    public synchronized Status status(Peer peer) {
        return peerGraveyard.contains(new DelayedPeer(peer, deadPeerWaitDuration)) ? Status.DEAD :
                peersVsStatus.getOrDefault(peer.getHostnameKey(), new Pair<>(peer, Status.UNDEFINED)).getValue();
    }

    public synchronized List<Peer> getIndirectPingPeers(Peer forPeer) {
        return peerIterator.getIndirectPingPeers(forPeer);
    }

    public synchronized List<Peer> getAlivePeers(int numberOfPeers) {
        return peerIterator.getPeers(Status.ALIVE, numberOfPeers);
    }

    public synchronized List<Peer> getAllDeadPeers() {
        return peerGraveyard.stream().map(delayedPeer -> delayedPeer.peer).collect(Collectors.toList());
    }

    public synchronized List<Peer> peers() {
        return Collections.unmodifiableList(peers);
    }

    public synchronized void addToDeadList(Peer peer) {
        peers.remove(peer);
        peersVsStatus.remove(peer.getHostnameKey());
        peerGraveyard.removeIf(delayedPeer -> delayedPeer.peer.getHostnameKey().equalsIgnoreCase(peer.getHostnameKey()));
        peerGraveyard.add(new DelayedPeer(peer, deadPeerWaitDuration));
        LOGGER.warn("Peer {} is now in the graveyard", peer.logFriendly());
    }

    //This could be tuned to be made sensitive to location of peers relative to each other
    public void neighbourify() {
        Collections.reverse(peers);
    }

    public synchronized Collection<Pair<Peer, Status>> statusMap() {
        return Collections.unmodifiableCollection(peersVsStatus.values());
    }

    private boolean ifPeerExists(Peer peer, Status newStatus) {
        Pair<Peer, Status> currentPair = peersVsStatus.get(peer.getHostnameKey());
        return currentPair != null && currentPair.getKey().equals(peer) && currentPair.getValue().equals(newStatus);
    }

    private boolean isOldIncarnation(Peer peer) {
        Pair<Peer, Status> currentPeerStatusPair = peersVsStatus.get(peer.getHostnameKey());
        if (currentPeerStatusPair != null) {
            Peer currentPeer = currentPeerStatusPair.getKey();
            return peer.getIncarnationInstant().isBefore(currentPeer.getIncarnationInstant()) || peer.getIncarnationNumber() < currentPeer.getIncarnationNumber();
        }
        return false;
    }

    private boolean isTheIdentityPeer(Peer peer) {
        return peer.getHostnameKey().equalsIgnoreCase(identityPeerService.identityPeer().getHostnameKey());
    }

    private boolean isADeadPeerBeingReAdded(Peer peer) {
        //The failure detector and the gossip disseminator can get caught in a race condition where the failure detector
        //re-adds a just removed host as a suspicious host. This goes on in infinite cycle and we need to short circuit that
        return peerGraveyard.contains(new DelayedPeer(peer, deadPeerWaitDuration)) ||
                peerGraveyard.stream()
                        .filter(delayedPeer -> delayedPeer.peer.getHostnameKey().equalsIgnoreCase(peer.getHostnameKey()))
                        .anyMatch(delayedPeer ->
                                delayedPeer.peer.getIncarnationNumber() > peer.getIncarnationNumber()
                                        || delayedPeer.peer.getIncarnationInstant().isAfter(peer.getIncarnationInstant()));
    }

    private class PeerIterator {
        private int pingPeerIteratorIndex = -1;
        private int gossipPeerIteratorIndex = -1;

        Optional<Peer> getNextPingPeer() {
            if (pingPeerIteratorIndex == peers.size() - 1) {
                pingPeerIteratorIndex = -1;
            }
            Peer peer = peers.size() > 0 ? peers.get(++pingPeerIteratorIndex % peers.size()) : null;
            return Optional.ofNullable(peer);
        }

        Optional<Peer> getNextGossipPeer() {
            if (gossipPeerIteratorIndex == peers.size() - 1) {
                gossipPeerIteratorIndex = -1;
            }
            Peer peer = PeerTracker.this.peers.size() > 0 ? PeerTracker.this.peers.get(++gossipPeerIteratorIndex % PeerTracker.this.peers.size()) : null;
            return Optional.ofNullable(peer);
        }

        List<Peer> getIndirectPingPeers(Peer forPeer) {
            return peers.stream()
                    .filter(pr -> !pr.equals(forPeer))
                    .filter(pr -> Status.ALIVE == peersVsStatus.get(pr.getHostnameKey()).getValue())
                    .limit(2)
                    .collect(Collectors.toList());
        }

        List<Peer> getPeers(Status status, int numberOfPeers) {
            return peers.stream()
                    .filter(pr -> status == peersVsStatus.get(pr.getHostnameKey()).getValue())
                    .limit(numberOfPeers)
                    .collect(Collectors.toList());
        }
    }

    class DelayedPeer implements Delayed {
        private final long expirationTime;
        private Peer peer;

        DelayedPeer(Peer peer, Duration delayDuration) {
            this.peer = peer;
            expirationTime = System.currentTimeMillis() + delayDuration.toMillis();
        }

        @Override
        public long getDelay(TimeUnit unit) {
            return unit.convert(expirationTime - System.currentTimeMillis(), TimeUnit.MILLISECONDS);
        }

        @Override
        public int compareTo(Delayed o) {
            return Long.valueOf(expirationTime - ((DelayedPeer) o).expirationTime).intValue();
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            DelayedPeer that = (DelayedPeer) o;
            return Objects.equals(peer, that.peer);
        }

        @Override
        public int hashCode() {

            return Objects.hash(peer);
        }
    }
}
