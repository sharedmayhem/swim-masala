package com.simple_software.swim_masala.supportactor;

public class NumberGenerator {
    private int gossipSequenceNumber = 0;
    private long roundNumber = 0;

    public synchronized int nextGossipSequenceNumber() {
        return ++gossipSequenceNumber;
    }

    public synchronized long nextRoundNumber() {
        return ++roundNumber;
    }

    public synchronized long getCurrentRoundNumber() {
        return roundNumber;
    }
}
