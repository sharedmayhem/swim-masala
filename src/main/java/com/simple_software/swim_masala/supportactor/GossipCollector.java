package com.simple_software.swim_masala.supportactor;

import com.simple_software.swim_masala.model.*;
import com.simple_software.swim_masala.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.function.Consumer;

/**
 * Collects all the gossip. Accessed by multiple threads and hence synchronized.
 * This will ensure only the latest incarnation of gossip is stored.
 */
public class GossipCollector {
    private static final Logger LOGGER = LoggerFactory.getLogger(GossipCollector.class);
    private final NumberGenerator numberGenerator;
    private final NodeClock nodeClock;
    private Consumer<Peer> deadPeerHandler;
    private IdentityPeerService identityPeerService;
    private Queue<AgedGossip> gossips = new PriorityBlockingQueue<>(50);
    private Map<String, Pair<Peer, Map<String, Gossip>>> gossipsMap = new HashMap<>();
    private int maximumAgeOfGossip;

    public GossipCollector(int maximumAgeOfGossip,
                           NumberGenerator numberGenerator,
                           NodeClock nodeClock,
                           Consumer<Peer> deadPeerHandler,
                           IdentityPeerService identityPeerService) {
        this.maximumAgeOfGossip = maximumAgeOfGossip;
        this.numberGenerator = numberGenerator;
        this.nodeClock = nodeClock;
        this.deadPeerHandler = deadPeerHandler;
        this.identityPeerService = identityPeerService;
    }

    public synchronized ADD_STATUS gossip(Gossip gossip) {
        return gossip(gossip, 0);
    }

    public synchronized Optional<Gossip> latestGossip() {
        AgedGossip agedGossip = gossips.poll();
        if (agedGossip != null) {
            gossip(agedGossip.getGossip(), agedGossip.currentAge() + 1);
            return Optional.of(agedGossip.getGossip());
        }

        return Optional.empty();
    }

    public synchronized List<Gossip> allTheLatestGossips() {
        List<Gossip> currentGossips = new ArrayList<>();
        List<AgedGossip> agedGossips = new ArrayList<>();
        AgedGossip agedGossip;
        while ((agedGossip = gossips.poll()) != null) {
            currentGossips.add(agedGossip.getGossip());
            agedGossips.add(agedGossip);
        }

        agedGossips.forEach(ag -> gossip(ag.getGossip(), ag.currentAge() + 1));
        return currentGossips;
    }

    public synchronized int size() {
        return gossips.size();
    }

    public synchronized ADD_STATUS gossip(Peer aboutPeer, Status newStatus) {
        return gossip(getGossip(aboutPeer, newStatus, identityPeerService.identityPeer()));
    }

    public Gossip getGossip(Peer aboutPeer, Status newStatus, Peer fromPeer) {
        return new GossipBuilder()
                .setAboutPeer(aboutPeer)
                .setBornAtTime(nodeClock.currentInstant())
                .setFromPeer(fromPeer)
                .setSequenceNumber(numberGenerator.nextGossipSequenceNumber())
                .setStatus(newStatus)
                .createGossip();
    }

    Map<String, Pair<Peer, Map<String, Gossip>>> getGossipsMap() {
        return gossipsMap;
    }

    synchronized ADD_STATUS gossip(Gossip gossip, long age) {

        boolean isFreshGossip = isFreshGossip(age);
        boolean isGossipLatestIncarnation = isGossipLatestIncarnation(gossip);

        if (isGossipLatestIncarnation && isFreshGossip && !isGossipAboutSelf(gossip)) {

            gossips.removeIf(ag -> (ag.getGossip().getGossipKey()).equals((gossip.getGossipKey())));
            gossips.add(new AgedGossip(gossip, age));

            gossipsMap.computeIfPresent(gossip.getAboutPeer().getHostnameKey(), (k, v) -> {
                v.getValue().put(gossip.getFromPeer().getHostnameKey(), gossip);
                return new Pair<>(gossip.getAboutPeer(), v.getValue());
            });
            return ADD_STATUS.ADDED;
        }

        if (!isFreshGossip) {
            removeFromGossipsMap(gossip);
            if (isGossipLatestIncarnation) {
                reviveGossipIfAboutSuspiciousPeer(gossip);
            }
        } else if (!isGossipLatestIncarnation) {
            removeFromGossipsMap(gossip);
            LOGGER.info("Ignoring gossip {} because it is old", gossip.logFriendly());
        }

        return ADD_STATUS.NOT_ADDED;
    }

    private boolean isGossipAboutSelf(Gossip gossip) {
        return identityPeerService.identityPeer().equals(gossip.getAboutPeer());
    }


    private void removeFromGossipsMap(Gossip gossip) {
        String aboutHostKey = gossip.getAboutPeer().getHostnameKey();
        gossipsMap.get(aboutHostKey).getValue().remove(gossip.getFromPeer().getHostnameKey());
    }


    private void reviveGossipIfAboutSuspiciousPeer(Gossip gossip) {
        if (gossip.getStatus() == Status.SUSPICIOUS) {
            LOGGER.info("I think peer {} is dead", gossip.getAboutPeer().logFriendly());
            gossip(gossip.getAboutPeer(), Status.DEAD);
            deadPeerHandler.accept(gossip.getAboutPeer());
        }
    }

    private boolean isFreshGossip(long age) {
        return age < maximumAgeOfGossip;
    }

    private boolean isGossipLatestIncarnation(Gossip newGossip) {
        String aboutHostnameKey = newGossip.getAboutPeer().getHostnameKey();
        Peer newAboutPeer = newGossip.getAboutPeer();

        Pair<Peer, Map<String, Gossip>> peerGossipsPair = gossipsMap.computeIfAbsent(aboutHostnameKey, k -> new Pair<>(newAboutPeer, new HashMap<>()));
        Peer oldAboutPeer = peerGossipsPair.getKey();

        if (newAboutPeer.compareTo(oldAboutPeer) > 0) {
            return true;
        } else if (newAboutPeer.compareTo(oldAboutPeer) == 0) {
            Gossip oldGossip = peerGossipsPair.getValue().computeIfAbsent(newGossip.getFromPeer().getHostnameKey(), k -> newGossip);
            return newGossip.getSequenceNumber() >= oldGossip.getSequenceNumber();
        } else {
            return false;
        }
    }

}
