package com.simple_software.swim_masala.model;

public enum Status {
    ALIVE, DEAD, SUSPICIOUS, UNDEFINED;
}
