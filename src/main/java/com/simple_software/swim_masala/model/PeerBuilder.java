package com.simple_software.swim_masala.model;

import java.time.Instant;
import java.util.Arrays;
import java.util.Objects;

public class PeerBuilder {
    private String hostname;
    private Instant incarnationInstant;
    private String type;
    private Integer pingReceiverPort;
    private Integer gossipReceiverPort;
    private Integer incarnationNumber;
    private String name;
    private String metadata;

    public PeerBuilder setIncarnationNumber(int incarnationNumber){
        this.incarnationNumber = incarnationNumber;
        return this;
    }

    public PeerBuilder setHostname(String hostname) {
        this.hostname = hostname;
        return this;
    }

    public PeerBuilder setIncarnationInstant(Instant incarnationInstant) {
        this.incarnationInstant = incarnationInstant;
        return this;
    }

    public PeerBuilder setType(String type) {
        this.type = type;
        return this;
    }

    public PeerBuilder setPingReceiverPort(int pingReceiverPort) {
        this.pingReceiverPort = pingReceiverPort;
        return this;
    }

    public PeerBuilder setGossipReceiverPort(int gossipReceiverPort) {
        this.gossipReceiverPort = gossipReceiverPort;
        return this;
    }

    public Peer createPeer() {
        requireNonNull(hostname, incarnationInstant, incarnationNumber, type, pingReceiverPort, gossipReceiverPort, name);
        return new Peer(hostname, incarnationInstant, incarnationNumber, type, pingReceiverPort, gossipReceiverPort, name, metadata);
    }

    public PeerBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public PeerBuilder setMetadata(String metadata) {
        this.metadata = metadata;
        return this;
    }

    private void requireNonNull(Object... objects){
        Arrays.stream(objects).forEach(Objects::requireNonNull);
    }

}