package com.simple_software.swim_masala.model;

import java.time.Instant;

public class NodeClock {
    public Instant currentInstant() {
        return Instant.now();
    }
}
