package com.simple_software.swim_masala.model;

public class AgedGossip implements Comparable<AgedGossip> {
    private Gossip gossip;
    private long currentAge;

    public AgedGossip(Gossip gossip, long age) {
        this.gossip = gossip;
        this.currentAge = age;
    }

    public long currentAge() {
        return currentAge;
    }

    public Gossip getGossip(){
        return gossip;
    }

    @Override
    public int compareTo(AgedGossip other) {
        int age = ((Long) (currentAge - other.currentAge())).intValue();
        if(age == 0){
            return ((Long)(other.getGossip().getSequenceNumber() - gossip.getSequenceNumber())).intValue();
        }
        return age;
    }
}
