package com.simple_software.swim_masala.model;

import java.time.Instant;

public class GossipBuilder {
    private Instant bornAtTime;
    private Peer fromPeer;
    private long sequenceNumber;
    private Peer aboutPeer;
    private Status status;


    public GossipBuilder setBornAtTime(Instant bornAtTime) {
        this.bornAtTime = bornAtTime;
        return this;
    }

    public GossipBuilder setFromPeer(Peer fromPeer) {
        this.fromPeer = fromPeer;
        return this;
    }

    public GossipBuilder setSequenceNumber(long sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
        return this;
    }

    public GossipBuilder setAboutPeer(Peer aboutPeer) {
        this.aboutPeer = aboutPeer;
        return this;
    }

    public GossipBuilder setStatus(Status status) {
        this.status = status;
        return this;
    }

    public Gossip createGossip() {
        return new Gossip(bornAtTime, fromPeer, sequenceNumber, aboutPeer, status);
    }

}