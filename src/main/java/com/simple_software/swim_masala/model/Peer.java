package com.simple_software.swim_masala.model;

import java.time.Instant;
import java.util.*;

public class Peer implements Comparable<Peer> {
    private final String hostname;
    private final Instant incarnationInstant;
    private final String type;
    private final int gossipReceiverPort;
    private final int pingReceiverPort;
    private String name;
    private String metadata;
    private int incarnationNumber;

    Peer(String hostname, Instant incarnationInstant, int incarnationNumber, String type, int pingReceiverPort, int gossipReceiverPort, String name, String metadata) {
        this.hostname = hostname;
        this.incarnationInstant = incarnationInstant;
        this.incarnationNumber = incarnationNumber;
        this.type = type;
        this.gossipReceiverPort = gossipReceiverPort;
        this.pingReceiverPort = pingReceiverPort;
        this.name = name;
        this.metadata = metadata;
    }


    public String getHostname() {
        return hostname;
    }

    public String getHostnameKey() {
        return hostname + ":" + pingReceiverPort;
    }

    public Instant getIncarnationInstant() {
        return incarnationInstant;
    }

    public String logFriendly() {
        return new StringJoiner("-").add(type).add(name).add(metadata).add(String.valueOf(incarnationNumber)).toString();
    }

    public String getType() {
        return type;
    }

    public int getGossipReceiverPort() {
        return gossipReceiverPort;
    }

    public int getPingReceiverPort() {
        return pingReceiverPort;
    }

    public int getIncarnationNumber() {
        return incarnationNumber;
    }

    @Override
    public int compareTo(Peer thatPeer) {
        if (thatPeer != null) {
            if (incarnationInstant.equals(thatPeer.incarnationInstant)) {
                return Integer.compare(incarnationNumber, thatPeer.incarnationNumber);
            } else {
                return incarnationInstant.isBefore(thatPeer.incarnationInstant) ? -1 : 1;
            }
        } else {
            return 1;
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Peer peer = (Peer) o;
        return Objects.equals(hostname, peer.hostname) &&
                Objects.equals(incarnationInstant, peer.incarnationInstant) &&
                Objects.equals(incarnationNumber, peer.incarnationNumber) &&
                Objects.equals(type, peer.type) &&
                Objects.equals(pingReceiverPort, peer.pingReceiverPort);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hostname, incarnationInstant, type);
    }

    public String asString() {
        return String.format("%s:%s:%s:%s:%d:%d:%d:%d", hostname, type, name, metadata, incarnationInstant.toEpochMilli(), incarnationNumber, pingReceiverPort, gossipReceiverPort);
    }

    public static Optional<Peer> fromString(String peerAsString) {
        List<String> messageComponents = Arrays.asList(peerAsString.split(":"));
        int index = 0;
        if (messageComponents.size() == 8) {
            String hostname = messageComponents.get(index++);
            String type = messageComponents.get(index++);
            String name = messageComponents.get(index++);
            String metadata = messageComponents.get(index++);
            Instant peerIncarnationInstant = Instant.ofEpochMilli(Long.valueOf(messageComponents.get(index++)));
            Integer incarnationNumber = Integer.valueOf(messageComponents.get(index++));
            Integer pingReceiverPort = Integer.valueOf(messageComponents.get(index++));
            Integer gossipReceiverPort = Integer.valueOf(messageComponents.get(index));
            return Optional.of(new PeerBuilder()
                    .setHostname(hostname)
                    .setIncarnationInstant(peerIncarnationInstant)
                    .setName(name)
                    .setType(type)
                    .setMetadata(metadata)
                    .setPingReceiverPort(pingReceiverPort)
                    .setGossipReceiverPort(gossipReceiverPort)
                    .setIncarnationNumber(incarnationNumber)
                    .createPeer());
        }
        return Optional.empty();
    }

    public String getName() {
        return name;
    }

    public String getMetadata() {
        return metadata;
    }

    @Override
    public String toString() {
        return "Peer{" +
                "hostname='" + hostname + '\'' +
                ", incarnationInstant=" + incarnationInstant +
                ", type='" + type + '\'' +
                ", name='" + name + '\'' +
                ", gossipReceiverPort=" + gossipReceiverPort +
                ", pingReceiverPort=" + pingReceiverPort +
                ", incarnationNumber=" + incarnationNumber +
                '}';
    }
}
