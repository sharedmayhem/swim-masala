package com.simple_software.swim_masala.model;

public enum ADD_STATUS {
    NOT_ADDED, ADDED
}
