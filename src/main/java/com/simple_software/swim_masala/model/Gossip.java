package com.simple_software.swim_masala.model;

import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class Gossip {
    //Instant at receiving host
    private Instant bornAtInstant;

    //sequence number at source host
    private long sequenceNumber;

    //From source host
    private Peer fromPeer;

    //From source host
    private Peer aboutPeer;

    //From source host
    private Status status;

    //Derived at receiving host
    private GossipKey gossipKey;

    Gossip(Instant bornAtInstant, Peer fromPeer, long sequenceNumber, Peer aboutPeer, Status status) {
        this.bornAtInstant = bornAtInstant;
        this.fromPeer = fromPeer;
        this.sequenceNumber = sequenceNumber;
        this.aboutPeer = aboutPeer;
        this.status = status;
        this.gossipKey = new GossipKey(this.fromPeer.getHostnameKey(), this.aboutPeer.getHostnameKey());
    }

    public GossipKey getGossipKey() {
        return gossipKey;
    }

    public Instant getBornAtInstant() {
        return bornAtInstant;
    }

    public Peer getFromPeer() {
        return fromPeer;
    }

    public long getSequenceNumber() {
        return sequenceNumber;
    }

    public Peer getAboutPeer() {
        return aboutPeer;
    }

    public Status getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return logFriendly();
    }

    public String logFriendly(){
        return new StringJoiner("-")
                .add("About:"+aboutPeer.logFriendly())
                .add("Status:"+status)
                .add("From:"+fromPeer.logFriendly())
                .add("Sequence:"+sequenceNumber)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Gossip gossip = (Gossip) o;
        return
                sequenceNumber == gossip.sequenceNumber &&
                        Objects.equals(fromPeer, gossip.fromPeer) &&
                        Objects.equals(aboutPeer, gossip.aboutPeer) &&
                        status == gossip.status;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bornAtInstant, fromPeer, sequenceNumber, aboutPeer, status);
    }

    public String asString() {
        return String.format("%s#%s#%s#%d", fromPeer.asString(), aboutPeer.asString(), status, sequenceNumber);
    }

    public static Gossip fromString(String gossipString) {
        String[] gossipComponents = gossipString.split("#");
        Peer fromPeer = Peer.fromString(gossipComponents[0]).orElseThrow(() -> new RuntimeException("Invalid from peer in " + gossipString));
        Peer aboutPeer = Peer.fromString(gossipComponents[1]).orElseThrow(() -> new RuntimeException("Invalid about peer in " + gossipString));
        Status status = Status.valueOf(gossipComponents[2]);
        Long incarnationNumber = Long.valueOf(gossipComponents[3]);
        return new GossipBuilder()
                .setStatus(status)
                .setSequenceNumber(incarnationNumber)
                .setBornAtTime(Instant.now())
                .setFromPeer(fromPeer)
                .setAboutPeer(aboutPeer)
                .createGossip();

    }

    public static String concatenatedGossips(List<Gossip> gossips) {
        return gossips.stream().map(Gossip::asString).collect(Collectors.joining("\n"));
    }

    public class GossipKey {
        private String fromHostKey;
        private String aboutHostKEY;

        GossipKey(String fromHostKey, String aboutHostKEY) {
            this.fromHostKey = fromHostKey;
            this.aboutHostKEY = aboutHostKEY;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GossipKey gossipKey = (GossipKey) o;
            return Objects.equals(fromHostKey, gossipKey.fromHostKey) &&
                    Objects.equals(aboutHostKEY, gossipKey.aboutHostKEY);
        }

        @Override
        public int hashCode() {
            return Objects.hash(fromHostKey, aboutHostKEY);
        }

        @Override
        public String toString() {
            return "GossipKey{" +
                    "fromHostKey='" + fromHostKey + '\'' +
                    ", aboutHostKEY='" + aboutHostKEY + '\'' +
                    '}';
        }
    }
}
