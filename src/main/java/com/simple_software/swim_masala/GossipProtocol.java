package com.simple_software.swim_masala;

import com.simple_software.swim_masala.actor.*;
import com.simple_software.swim_masala.lossy.LossyGossipReceiver;
import com.simple_software.swim_masala.lossy.LossyPingReceiver;
import com.simple_software.swim_masala.model.NetworkConstants;
import com.simple_software.swim_masala.model.NodeClock;
import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.PeerBuilder;
import com.simple_software.swim_masala.supportactor.*;
import com.simple_software.swim_masala.util.NetworkQueryResolver;
import com.simple_software.swim_masala.util.UDPCommunicator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.DatagramSocket;
import java.net.ServerSocket;
import java.time.Instant;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * This class is the main assembler for the protocol.
 */
public class GossipProtocol {

    private static final Logger LOGGER = LoggerFactory.getLogger(GossipProtocol.class);
    private IdentityPeerService identityPeerService;
    private NetworkQueryResolver networkQueryResolver;
    private GossipCollector gossipCollector;
    private PeerTracker peerTracker;
    private Pinger pinger;
    private FailureDetector failureDetector;
    private GossipDisseminator gossipDisseminator;
    private PingReceiver pingReceiver;
    private GossipReceiver gossipReceiver;
    private ExecutorService receiverService;
    private Time time = new Time();

    public static void main(String[] args) {
        GossipProtocol gossipProtocol = new GossipProtocol();
        gossipProtocol.start();
        String parent = System.getProperty("parent");
        String port = System.getProperty("port");
        if (parent != null && port != null) {
            gossipProtocol.talkToParent(parent, port);
        }
    }

    //Called from library user
    public void start() {
        start(peer -> {
        });
    }

    //Called from library user
    public void start(Consumer<Peer> onDeadPeer) {
        start(onDeadPeer, p -> {});
    }

    //Called from library user
    public void start(Consumer<Peer> onDeadPeer, Consumer<Peer> onAlivePeer) {
        initActors(onDeadPeer, onAlivePeer);
        schedule();
    }

    //Called from library user
    public void talkToParent(String hostname, String pingPort) {
        if (Objects.nonNull(hostname) && Objects.nonNull(pingPort)) {
            try {
                Infant infant = new Infant(gossipCollector, peerTracker, identityPeerService, gossipDisseminator);
                infant.buildWorldViewByAsking(networkQueryResolver.getSocket(hostname, Integer.valueOf(pingPort)));
            } catch (Exception e) {
                LOGGER.error("Could not connect to specified peer. Exiting.", e);
                System.exit(-1);
            }
        }
    }

    public PeerTracker getPeerTracker() {
        return peerTracker;
    }

    void initActors(Consumer<Peer> onDeadPeer, Consumer<Peer> onAlivePeer) {
        networkQueryResolver = new NetworkQueryResolver();
        ServerSocket pingReceiverSocket = getPingReceiverSocket();
        DatagramSocket gossipReceiverSocket = networkQueryResolver.getDatagramSocket();
        identityPeerService = new IdentityPeerService(new PeerBuilder()
                .setHostname(networkQueryResolver.getHostname())
                .setIncarnationInstant(Instant.now())
                .setType(System.getProperty("peer-type", "standard"))
                .setName(System.getProperty("peer-name", "nameless"))
                .setMetadata(System.getProperty("metadata", "none"))
                .setPingReceiverPort(pingReceiverSocket.getLocalPort())
                .setGossipReceiverPort(gossipReceiverSocket.getLocalPort())
                .setIncarnationNumber(1)
                .createPeer());

        pinger = new Pinger(time.pingTimeOut(), time.indirectPingTimeOut());
        NumberGenerator numberGenerator = new NumberGenerator();
        NodeClock nodeClock = new NodeClock();
        UDPCommunicator udpCommunicator = new UDPCommunicator(NetworkConstants.DATAGRAM_PACKET_CONTENT_SIZE_LIMIT);

        peerTracker = new PeerTracker(identityPeerService, time.deadPeerWaitDuration(), onDeadPeer, onAlivePeer);
        gossipCollector = new GossipCollector(2, numberGenerator, nodeClock, peerTracker::addToDeadList, identityPeerService);
        failureDetector = new FailureDetector(pinger, peerTracker, gossipCollector, identityPeerService);
        gossipDisseminator = new GossipDisseminator(udpCommunicator, peerTracker, gossipCollector, identityPeerService);

        Boolean isLossy = Boolean.valueOf(System.getProperty("lossy", "false"));
        if (isLossy) {
            Float lossyProbability = Float.valueOf(System.getProperty("lossy-probability", "0"));
            pingReceiver = new LossyPingReceiver(pingReceiverSocket, getExecutorService(1, "PingServicer"), peerTracker, gossipCollector, pinger, lossyProbability);
            gossipReceiver = new LossyGossipReceiver(gossipReceiverSocket, peerTracker, gossipCollector, getExecutorService(1, "GossipReceiver"), gossipDisseminator, lossyProbability);
        } else {
            pingReceiver = new PingReceiver(pingReceiverSocket, getExecutorService(1, "PingServicer"), peerTracker, gossipCollector, pinger);
            gossipReceiver = new GossipReceiver(gossipReceiverSocket, peerTracker, gossipCollector, getExecutorService(1, "GossipReceiver"), gossipDisseminator);
        }


        LOGGER.info("Hostname is {}", networkQueryResolver.getHostname());
        LOGGER.info("Receiving gossip on port {}", gossipReceiverSocket.getLocalPort());
        LOGGER.info("Receiving pings on port {}", pingReceiverSocket.getLocalPort());

    }

    IdentityPeerService getIdentityPeerService() {
        return identityPeerService;
    }

    GossipDisseminator getGossipDisseminator() {
        return gossipDisseminator;
    }

    GossipCollector getGossipCollector() {
        return gossipCollector;
    }

    void schedule() {
        ScheduledExecutorService failureDetectorService = getScheduledExecutorService("FailureDetector");
        schedule(failureDetector::detectFailure, 5, Long.valueOf(time.failureDetectorFrequency().getSeconds()).intValue(), TimeUnit.SECONDS, failureDetectorService);

        ScheduledExecutorService gossipDisseminatorService = getScheduledExecutorService("GossipDisseminator");
        schedule(gossipDisseminator::disseminate, 10, Long.valueOf(time.gossipDisseminatorDetectorFrequency().toMillis()).intValue(), TimeUnit.MILLISECONDS, gossipDisseminatorService);

        receiverService = getExecutorService(2, "ReceiverService");
        receiverService.execute(pingReceiver::start);
        receiverService.execute(gossipReceiver::start);

        Runtime.getRuntime().addShutdownHook(new Thread(() -> new Leaver(Arrays.asList(receiverService, failureDetectorService, gossipDisseminatorService), peerTracker, pinger).leave(identityPeerService)));
    }

    private ServerSocket getPingReceiverSocket() {
        if (Boolean.valueOf(System.getProperty("test-mode", "false"))) {
            return networkQueryResolver.getServerSocket(60000);
        }
        return networkQueryResolver.getServerSocket();
    }

    private void schedule(Runnable runnable, int initialDelay, int frequency, TimeUnit timeUnit, ScheduledExecutorService scheduledExecutorService) {
        scheduledExecutorService.scheduleAtFixedRate(runnable, initialDelay, frequency, timeUnit);
    }

    private ScheduledExecutorService getScheduledExecutorService(String name) {
        return Executors.newScheduledThreadPool(1, r -> {
            Thread thread = new Thread(r);
            thread.setName(name);
            thread.setUncaughtExceptionHandler((t, e) -> LOGGER.error("Exception in thread {}: {}", t, e));
            return thread;
        });
    }

    private ExecutorService getExecutorService(int noOfThreads, String name) {
        return Executors.newFixedThreadPool(noOfThreads, r -> {
            Thread thread = new Thread(r);
            thread.setName(name);
            thread.setUncaughtExceptionHandler((t, e) -> LOGGER.error("Exception in thread {}: {}", t, e));
            return thread;
        });
    }

}
