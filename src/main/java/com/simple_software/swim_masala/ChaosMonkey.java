package com.simple_software.swim_masala;

import com.simple_software.swim_masala.actor.Infant;
import com.simple_software.swim_masala.model.Gossip;
import com.simple_software.swim_masala.model.GossipBuilder;
import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.util.NetworkQueryResolver;
import com.simple_software.swim_masala.util.Pair;
import com.simple_software.swim_masala.util.TCPCommunicator;
import com.simple_software.swim_masala.util.UDPCommunicator;

import java.io.IOException;
import java.net.Socket;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.*;

/**
 * This class can send malicious gossips to peers in a cluster.
 * It does so by publishing gossips to targeted peers and not by being a part of the cluster
 */
public class ChaosMonkey {
    private final Command NULL = new Command() {

        @Override
        void process(String input) {
            //Do nothing
        }
    };
    private Map<Peer, Status> peers = new TreeMap<>((o1, o2) -> {
        Instant o1Instant = o1.getIncarnationInstant();
        Instant o2Instant = o2.getIncarnationInstant();
        return o1Instant.isBefore(o2Instant) ? -1 : (o1Instant.isAfter(o2Instant) ? 1 : 0);
    });
    private UDPCommunicator udpCommunicator = new UDPCommunicator(1400);
    private TCPCommunicator tcpCommunicator = new TCPCommunicator();
    private NetworkQueryResolver networkQueryResolver = new NetworkQueryResolver();

    public static void main(String[] args) {
        ChaosMonkey chaosMonkey = new ChaosMonkey();
        chaosMonkey.letThereBeChaos();

    }

    public Optional<Peer> getPeer(Status status) {
        return peers.entrySet().stream().filter(entry -> entry.getValue().equals(Status.ALIVE)).map(entry -> entry.getKey()).findFirst();
    }

    private void letThereBeChaos() {
        System.out.println("*************************************************************************************");
        System.out.println("I am THE Chaos Monkey. Practicing my freedom of speech since " + LocalDateTime.now());
        System.out.println("*************************************************************************************");
        Scanner scanner = new Scanner(System.in).useDelimiter("\n");
        System.out.print(LocalDateTime.now() + ":");
        while (scanner.hasNext() && !Thread.currentThread().isInterrupted()) {
            String input = scanner.next();
            Command command = commandFactory(input);
            try {
                command.process(input);
            } catch (Exception e) {
                System.out.println("Error parsing command!");
            }
            System.out.print(LocalDateTime.now() + ":");

        }
    }

    private Command commandFactory(String input) {
        switch (input.split(" ")[0].toUpperCase()) {
            case "GOSSIP":
                return new MaliciousGossiper();
            case "BUILD":
                return new WorldViewBuilder();
            case "LIST":
                return new PeerLister();
        }
        return NULL;
    }

    abstract class Command {
        abstract void process(String input);
    }

    //gossip <peer metadata> <status>
    class MaliciousGossiper extends Command {

        @Override
        void process(String input) {
            String[] split = input.split(" ");
            String aboutPeerMetadata = split[1];
            Status status = Status.valueOf(split[2].toUpperCase());
            String toPeerMetadata = split[3];
            Optional<Peer> optionalAboutPeer = getPeer(aboutPeerMetadata);
            Optional<Peer> optionalToPeer = getPeer(toPeerMetadata);

            if (optionalAboutPeer.isPresent() && optionalToPeer.isPresent()) {
                Peer aboutPeer = optionalAboutPeer.get();
                Peer toPeer = optionalToPeer.get();
                udpCommunicator.send(toPeer.getHostname(), toPeer.getGossipReceiverPort(), Gossip.concatenatedGossips(Collections.singletonList(getGossip(aboutPeer, status))));
            } else {
                System.out.println("Check peer metadata: " + aboutPeerMetadata + ", " + toPeerMetadata);
            }
        }

        private Optional<Peer> getPeer(String type) {
            return
                    peers.keySet()
                            .stream()
                            .filter(pr -> pr.getType().equalsIgnoreCase(type))
                            .findFirst();
        }

        private Gossip getGossip(Peer aboutPeer, Status status) {
            return new GossipBuilder()
                    .setAboutPeer(aboutPeer)
                    .setBornAtTime(Instant.now())
                    .setFromPeer(aboutPeer)
                    .setSequenceNumber(999)
                    .setStatus(status)
                    .createGossip();
        }
    }

    class WorldViewBuilder extends Command {

        @Override
        void process(String input) {
            peers = new HashMap<>();
            try (Socket socket = networkQueryResolver.getSocket(System.getProperty("parent"), Integer.valueOf(System.getProperty("port")))) {
                tcpCommunicator.writeLine("list-peers:", socket);
                List<String> result = tcpCommunicator.readLines(socket.getInputStream());
                for (String peerAndStatus : result) {
                    Pair<Peer, Status> pair = Infant.mapToPeerStatusPair(peerAndStatus);
                    peers.put(pair.getKey(), pair.getValue());
                }
                System.out.println("There are " + peers.size() + " peers in the cluster.");
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }


    private class PeerLister extends Command {
        @Override
        void process(String input) {
            System.out.println("Peers : " + peers.size());
            peers
                    .entrySet()
                    .stream()
                    .map(entry -> entry.getKey().logFriendly() + ":" + entry.getValue())
                    .forEach(System.out::println);
        }
    }
}
