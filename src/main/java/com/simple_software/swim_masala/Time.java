package com.simple_software.swim_masala;

import java.time.Duration;

public class Time {

    public Duration deadPeerWaitDuration(){
        return Duration.ofSeconds(10);
    }

    public Duration indirectPingTimeOut(){
        return Duration.ofSeconds(20);
    }

    public Duration pingTimeOut(){
        return Duration.ofSeconds(10);
    }

    public Duration failureDetectorFrequency(){
        return Duration.ofSeconds(2);
    }

    public Duration gossipDisseminatorDetectorFrequency(){
        return Duration.ofMillis(200);
    }

    public static class DebuggerTime extends Time {
        @Override
        public Duration deadPeerWaitDuration() {
            return Duration.ofHours(1);
        }

        @Override
        public Duration indirectPingTimeOut() {
            return Duration.ofHours(1);
        }

        @Override
        public Duration pingTimeOut() {
            return Duration.ofHours(1);
        }

        @Override
        public Duration failureDetectorFrequency() {
            return Duration.ofHours(1);
        }

        @Override
        public Duration gossipDisseminatorDetectorFrequency() {
            return super.gossipDisseminatorDetectorFrequency();
        }
    }
}
