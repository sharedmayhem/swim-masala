package com.simple_software.swim_masala.lossy;

import com.simple_software.swim_masala.actor.GossipDisseminator;
import com.simple_software.swim_masala.actor.GossipReceiver;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.concurrent.ExecutorService;

public class LossyGossipReceiver extends GossipReceiver implements LossyReceiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(LossyGossipReceiver.class);

    private final int numberOfMessagesToRespondTo;
    private final int numberOfMessagesToDrop;

    public LossyGossipReceiver(
            DatagramSocket datagramSocket,
            PeerTracker peerTracker,
            GossipCollector gossipCollector,
            ExecutorService gossipReceiverService,
            GossipDisseminator gossipDisseminator,
            Float lossyProbability) {
        super(datagramSocket, peerTracker, gossipCollector, gossipReceiverService, gossipDisseminator);

        numberOfMessagesToRespondTo = Float.valueOf((1 - lossyProbability) * 100).intValue();
        numberOfMessagesToDrop = 100 - numberOfMessagesToRespondTo;
    }


    @Override
    protected void processPacket(DatagramPacket packet) {
        longAdder.add(1);
        long numberOfMessagesProcessed = longAdder.sum();
        if (numberOfMessagesProcessed < numberOfMessagesToRespondTo) {
            super.processPacket(packet);
        } else if (numberOfMessagesProcessed >= 100) {
            resetAdder();
            LOGGER.info("Lossiness cured");
        }

    }


}
