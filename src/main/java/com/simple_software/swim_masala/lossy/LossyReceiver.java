package com.simple_software.swim_masala.lossy;

import java.util.concurrent.atomic.LongAdder;

public interface LossyReceiver {
    LongAdder longAdder = new LongAdder();

    default void resetAdder() {
        longAdder.reset();
    }
}
