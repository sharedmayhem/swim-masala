package com.simple_software.swim_masala.lossy;

import com.simple_software.swim_masala.actor.PingReceiver;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.supportactor.Pinger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

public class LossyPingReceiver extends PingReceiver implements LossyReceiver {

    private final int numberOfMessagesToRespondTo;
    private final int numberOfMessagesToDrop;
    private final Logger LOGGER = LoggerFactory.getLogger(LossyPingReceiver.class);

    public LossyPingReceiver(
            ServerSocket serverSocket,
            ExecutorService executorService,
            PeerTracker peerTracker,
            GossipCollector gossipCollector,
            Pinger pinger,
            Float lossyProbability) {
        super(serverSocket, executorService, peerTracker, gossipCollector, pinger);
        if (lossyProbability > 1) {
            throw new RuntimeException("Lossy probability must be be less than 1");
        }

        numberOfMessagesToRespondTo = Float.valueOf((1 - lossyProbability) * 100).intValue();
        numberOfMessagesToDrop = 100 - numberOfMessagesToRespondTo;
    }


    @Override
    protected AbstractCommand getCommand(String data, Socket serverConnectionToClient) {

        if (data.startsWith("ack-req")) {
            return new LossyAckRequestCommand(data, serverConnectionToClient);
        }

        return super.getCommand(data, serverConnectionToClient);

    }


    private class LossyAckRequestCommand extends AckRequestCommand {
        LossyAckRequestCommand(String data, Socket serverConnectionToClient) {
            super(data, serverConnectionToClient);
        }

        @Override
        public void process() throws Exception {
            longAdder.add(1);
            long numberOfMessagesProcessed = longAdder.sum();
            if (numberOfMessagesProcessed < numberOfMessagesToRespondTo) {
                super.process();
            } else if (numberOfMessagesProcessed >= 100) {
                resetAdder();
                LOGGER.info("Lossiness cured");
            }
        }
    }


}


