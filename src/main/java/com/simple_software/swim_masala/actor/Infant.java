package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.util.Pair;
import com.simple_software.swim_masala.util.TCPCommunicator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.Socket;
import java.util.List;

/**
 * This class makes the first call to any other peer in the cluster to download its world view.
 * Upon receiving the world view, it will gossip about it to its new 'friends'.
 */
public class Infant {
    private static final Logger LOGGER = LoggerFactory.getLogger(Infant.class);
    private final TCPCommunicator tcpCommunicator;
    private final GossipCollector gossipCollector;
    private final PeerTracker peerTracker;
    private IdentityPeerService identityPeerService;
    private GossipDisseminator gossipDisseminator;

    public Infant(GossipCollector gossipCollector, PeerTracker peerTracker, IdentityPeerService identityPeerService, GossipDisseminator gossipDisseminator) {
        this.identityPeerService = identityPeerService;
        this.gossipDisseminator = gossipDisseminator;
        this.tcpCommunicator = new TCPCommunicator();
        this.gossipCollector = gossipCollector;
        this.peerTracker = peerTracker;
    }


    public void buildWorldViewByAsking(Socket socket) {
        tcpCommunicator.writeLine("goo-goo:" + identityPeerService.identityPeer().asString(), socket);
        try {
            List<String> fromPeer = tcpCommunicator.readLines(socket.getInputStream());
            for (String peerAndStatus : fromPeer) {
                Pair<Peer, Status> pair = mapToPeerStatusPair(peerAndStatus);
                peerTracker.add(pair.getKey(), pair.getValue());
                gossipCollector.gossip(pair.getKey(), pair.getValue());
                LOGGER.info("Peer added {}", pair.getKey().logFriendly());
            }
            peerTracker.neighbourify();
            gossipDisseminator.tellFactsAboutMyOwnWellBeing();

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static Pair<Peer, Status> mapToPeerStatusPair(String peerAndStatus) {
        int lastIndexOfSemiColon = peerAndStatus.lastIndexOf(":");
        Peer peer = Peer.fromString(peerAndStatus.substring(0, lastIndexOfSemiColon)).orElseThrow(() -> new RuntimeException("Invalid response received by infant: " + peerAndStatus));
        Status status = Status.valueOf(peerAndStatus.substring(lastIndexOfSemiColon + 1, peerAndStatus.length()));
        return new Pair<>(peer, status);
    }
}
