package com.simple_software.swim_masala.actor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;

public abstract class AbstractTCPReceiver {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractTCPReceiver.class);
    private ServerSocket serverSocket;
    private ExecutorService executorService;

    AbstractTCPReceiver(ServerSocket serverSocket, ExecutorService executorService) {
        this.serverSocket = serverSocket;
        this.executorService = executorService;
    }

    public boolean start() {
        try {
            while (!Thread.currentThread().isInterrupted()) {
                Socket connection = serverSocket.accept();
                executorService.execute(() -> processSocket(connection));
            }
            return true;
        } catch (IOException e) {
            LOGGER.error("Exception", e);
            return false;
        }
    }

    protected abstract void processSocket(Socket socket) ;
}
