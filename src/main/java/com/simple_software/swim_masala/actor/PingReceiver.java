package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.ADD_STATUS;
import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.supportactor.Pinger;
import com.simple_software.swim_masala.util.NetworkQueryResolver;
import com.simple_software.swim_masala.util.Pair;
import com.simple_software.swim_masala.util.TCPCommunicator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.ExecutorService;

import static java.lang.String.format;

/**
 * This class receives the ping requests, indirect ping requests, infant requests and requests to list its peers.
 * All requests come over TCP
 */
public class PingReceiver extends AbstractTCPReceiver {
    private final Logger LOGGER = LoggerFactory.getLogger(getClass());
    private final TCPCommunicator tcpCommunicator;
    private final NetworkQueryResolver networkQueryResolver;
    private final PeerTracker peerTracker;
    private final GossipCollector gossipCollector;
    private final Pinger pinger;

    public PingReceiver(
            ServerSocket serverSocket,
            ExecutorService executorService,
            PeerTracker peerTracker,
            GossipCollector gossipCollector,
            Pinger pinger) {
        super(serverSocket, executorService);
        this.peerTracker = peerTracker;
        this.gossipCollector = gossipCollector;
        this.pinger = pinger;
        tcpCommunicator = new TCPCommunicator();
        networkQueryResolver = new NetworkQueryResolver();
    }

    @Override
    protected void processSocket(Socket socket) {
        try (InputStream inputStream = socket.getInputStream();
             OutputStream outputStream = socket.getOutputStream()) {
            getCommand(tcpCommunicator.readLine(inputStream), socket).process();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


    protected AbstractCommand getCommand(String data, Socket serverConnectionToClient) {

        if (data.startsWith("ack-req")) {
            return new AckRequestCommand(data, serverConnectionToClient);
        }

        if (data.startsWith("ackRequest-req")) {
            return new IndirectPingRequestCommand(data, serverConnectionToClient);
        }

        if (data.startsWith("goo-goo")) {
            return new GooGooGaaGaaCommand(data, serverConnectionToClient);
        }

        if (data.startsWith("bye-bye")) {
            return new LeaveRequestCommand(data, serverConnectionToClient);
        }

        if (data.startsWith("list-peers")) {
            return new ListPeers(data, serverConnectionToClient);
        }

        throw new RuntimeException("Unrecognized command received at ackRequest receiver");

    }

    protected abstract class AbstractCommand {
        protected String data;
        Socket serverConnectionToClient;

        AbstractCommand(String data, Socket serverConnectionToClient) {
            this.data = data;
            this.serverConnectionToClient = serverConnectionToClient;
        }

        abstract void process() throws Exception;

        void updateWorldView(Peer aboutPeer, Status newStatus) {
            ADD_STATUS addStatus = peerTracker.add(aboutPeer, newStatus);
            if (addStatus == ADD_STATUS.ADDED) {
                ADD_STATUS gossipAddStatus = gossipCollector.gossip(aboutPeer, newStatus);
                LOGGER.info("Peer added {}. Status {}. Gossip added? {}", aboutPeer.logFriendly(), newStatus, gossipAddStatus);
            }
        }

    }

    protected class AckRequestCommand extends AbstractCommand {
        protected AckRequestCommand(String data, Socket targetHost) {
            super(data, targetHost);
        }

        @Override
        public void process() throws Exception {
            Peer fromPeer = Peer.fromString(data.substring(data.indexOf(":") + 1, data.length())).orElseThrow(() -> new RuntimeException("Invalid ack request"));
            tcpCommunicator.writeLine(format("%s:%s", networkQueryResolver.getHostname(), Status.ALIVE), serverConnectionToClient);
            if (peerTracker.status(fromPeer) != Status.ALIVE) {
                gossipCollector.gossip(fromPeer, Status.SUSPICIOUS);
            }
        }
    }

    class LeaveRequestCommand extends AbstractCommand {
        private final Peer peer;

        LeaveRequestCommand(String data, Socket targetHost) {
            super(data, targetHost);
            peer = Peer.fromString(data.substring(data.indexOf(":") + 1, data.length())).orElseThrow(() -> new RuntimeException("Invalid leave request"));
        }

        @Override
        public void process() throws Exception {
            if (isPhishingRequest(serverConnectionToClient)) {
                tcpCommunicator.writeLine("fail", serverConnectionToClient);
                LOGGER.info("Attempt to phish made by {} on behalf of {}",
                        serverConnectionToClient.getInetAddress().getHostName() + ":" + serverConnectionToClient.getPort(), peer.asString());
            } else {
                updateWorldView(peer, Status.DEAD);
                tcpCommunicator.writeLine("ok", serverConnectionToClient);
                LOGGER.info("Peer {} said bye-bye. Good riddance!", peer.logFriendly());
            }
        }

        private boolean isPhishingRequest(Socket targetHost) {
            try {
                return !targetHost.getInetAddress().equals(networkQueryResolver.getByName(peer.getHostname()));
            } catch (Exception e) {
                return true;
            }
        }
    }

    class IndirectPingRequestCommand extends AbstractCommand {
        private final Peer peer;

        IndirectPingRequestCommand(String data, Socket serverConnectionToClient) {
            super(data, serverConnectionToClient);
            peer = Peer.fromString(data.substring(data.indexOf(":") + 1, data.length())).orElseThrow(() -> new RuntimeException("Invalid indirect ackRequest request"));
        }

        @Override
        public void process() {
            String statusTemplate = peer.getHostname() + ":%s";
            tcpCommunicator.writeLine(pinger.ackRequest(peer, peerTracker.getIdentityPeer()) ? format(statusTemplate, Status.ALIVE) : format(statusTemplate, Status.SUSPICIOUS), serverConnectionToClient);
        }
    }

    class GooGooGaaGaaCommand extends AbstractCommand {

        private final Peer aboutPeer;

        GooGooGaaGaaCommand(String data, Socket serverConnectionToClient) {
            super(data, serverConnectionToClient);
            aboutPeer = Peer.fromString(data.substring(data.indexOf(":") + 1, data.length())).orElseThrow(() -> new RuntimeException("Invalid baby talk"));
        }

        @Override
        public void process() {
            updateWorldView(aboutPeer, Status.ALIVE);
            List<String> peerTrackerAsString = new ArrayList<>();
            Collection<Pair<Peer, Status>> statusMap = peerTracker.statusMap();
            peerTrackerAsString.add(asString(peerTracker.getIdentityPeer(), Status.ALIVE));
            statusMap
                    .stream()
                    .filter(pair -> !pair.getKey().getHostnameKey().equalsIgnoreCase(aboutPeer.getHostnameKey()))
                    .map(pair -> asString(pair.getKey(), pair.getValue()))
                    .forEach(peerTrackerAsString::add);
            tcpCommunicator.writeLines(peerTrackerAsString, serverConnectionToClient);
            LOGGER.info("Welcome peer {}", aboutPeer.logFriendly());

        }

        private String asString(Peer peer, Status status) {
            return format("%s:%s", peer.asString(), status);
        }

    }

    class ListPeers extends AbstractCommand {

        ListPeers(String data, Socket serverConnectionToClient) {
            super(data, serverConnectionToClient);
        }

        @Override
        void process() throws Exception {
            List<String> peerTrackerAsString = new ArrayList<>();
            Collection<Pair<Peer, Status>> statusMap = peerTracker.statusMap();
            peerTrackerAsString.add(asString(peerTracker.getIdentityPeer(), Status.ALIVE));
            statusMap
                    .stream()
                    .map(pair -> asString(pair.getKey(), pair.getValue()))
                    .forEach(peerTrackerAsString::add);
            tcpCommunicator.writeLines(peerTrackerAsString, serverConnectionToClient);
        }

        private String asString(Peer peer, Status status) {
            return format("%s:%s", peer.asString(), status);
        }
    }
}


