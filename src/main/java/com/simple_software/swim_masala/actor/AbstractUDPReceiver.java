package com.simple_software.swim_masala.actor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.nio.charset.Charset;
import java.util.concurrent.ExecutorService;

public abstract class AbstractUDPReceiver {
    private DatagramSocket datagramSocket;
    private ExecutorService gossipReceiverService;
    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractUDPReceiver.class);

    public AbstractUDPReceiver(DatagramSocket datagramSocket, ExecutorService gossipReceiverService) {
        this.datagramSocket = datagramSocket;
        this.gossipReceiverService = gossipReceiverService;
    }

    public void start() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                byte[] buf = new byte[1500];
                DatagramPacket packet = new DatagramPacket(buf, buf.length);
                datagramSocket.receive(packet);
                gossipReceiverService.execute(() -> processPacket(packet));
            } catch (Exception e) {
                LOGGER.error("Exception:", e);
            }
        }
    }

    protected abstract void processPacket(DatagramPacket packet);

    String getData(DatagramPacket packet) {
        byte[] actualDataThatIsPopulatedInPacket = new byte[packet.getLength()];
        byte[] packetData = packet.getData();
        System.arraycopy(packetData, 0, actualDataThatIsPopulatedInPacket, 0, packet.getLength());
        return new String(actualDataThatIsPopulatedInPacket, Charset.forName("UTF-8"));
    }
}
