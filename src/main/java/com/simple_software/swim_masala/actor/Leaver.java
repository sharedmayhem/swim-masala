package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.supportactor.Pinger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

/**
 * This class will send a goodbye message to atleast 2 peers. This is called when the peer intends to leave the cluster
 */
public class Leaver {
    private static final Logger LOGGER = LoggerFactory.getLogger(Leaver.class);
    private final List<ExecutorService> executorServices;
    private final PeerTracker peerTracker;
    private Pinger pinger;

    public Leaver(List<ExecutorService> executorServices, PeerTracker peerTracker, Pinger pinger) {
        this.executorServices = executorServices;
        this.peerTracker = peerTracker;
        this.pinger = pinger;
    }

    public void leave(IdentityPeerService identityPeerService) {
        executorServices.forEach(ExecutorService::shutdownNow);
        List<Peer> alivePeers = peerTracker.getAlivePeers(2);
        alivePeers.stream().parallel().forEach(peer -> pinger.sayByeTo(peer, identityPeerService.identityPeer()));
        LOGGER.info("\nLeaving the cluster with tears in my eyes. Goodbye my peers {}. \nWith love,\n{}",
                alivePeers.stream().map(Peer::logFriendly).collect(Collectors.joining(",")),
                identityPeerService.identityPeer().logFriendly());

    }
}
