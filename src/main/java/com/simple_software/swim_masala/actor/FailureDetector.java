package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.ADD_STATUS;
import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.supportactor.Pinger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

/**
 * This will ping/indirect ping other peers in a round robin fashion of the cluster to determine whether they are alive.
 * If a peer has changed status, then update the local gossip collector
 */
public class FailureDetector {

    private static final Logger LOGGER = LoggerFactory.getLogger(FailureDetector.class);

    private Pinger pinger;
    private PeerTracker peerTracker;
    private GossipCollector gossipCollector;
    private IdentityPeerService identityPeerService;

    public FailureDetector(
            Pinger pinger,
            PeerTracker peerTracker,
            GossipCollector gossipCollector,
            IdentityPeerService identityPeerService) {
        this.pinger = pinger;
        this.peerTracker = peerTracker;
        this.gossipCollector = gossipCollector;
        this.identityPeerService = identityPeerService;
    }


    public void detectFailure() {
        Optional<Peer> optionalPingPeer = peerTracker.getPingPeer();
        optionalPingPeer.ifPresent(this::ping);
    }

    private void ping(Peer pingPeer) {
        ADD_STATUS peerAddStatus = peerTracker.add(pingPeer, isPeerAlive(pingPeer) ? Status.ALIVE : Status.SUSPICIOUS);
        if (peerAddStatus == ADD_STATUS.ADDED) {
            Status status = peerTracker.status(pingPeer);
            gossipCollector.gossip(pingPeer, status);
            LOGGER.info("Peer {} is {}", pingPeer.logFriendly(), status);
        }
    }

    private boolean isPeerAlive(Peer pingPeer) {
        return (pinger.ackRequest(pingPeer, identityPeerService.identityPeer()) || pinger.indirectPing(pingPeer, peerTracker.getIndirectPingPeers(pingPeer)));
    }

}
