package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.ADD_STATUS;
import com.simple_software.swim_masala.model.Gossip;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.util.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;

/**
 * This class will receive gossip from other peers and update the peer tracker accordingly.
 * If it finds its own state is either suspicious/dead, then it will call upon the gossip
 * disseminator to gossip about its own well being
 */
public class GossipReceiver extends AbstractUDPReceiver {
    private static final Logger LOGGER = LoggerFactory.getLogger(GossipReceiver.class);
    private final PeerTracker peerTracker;
    private final GossipCollector gossipCollector;
    private final GossipDisseminator gossipDisseminator;

    public GossipReceiver(
            DatagramSocket datagramSocket,
            PeerTracker peerTracker,
            GossipCollector gossipCollector,
            ExecutorService gossipReceiverService,
            GossipDisseminator gossipDisseminator) {
        super(datagramSocket, gossipReceiverService);
        this.peerTracker = peerTracker;
        this.gossipCollector = gossipCollector;
        this.gossipDisseminator = gossipDisseminator;
    }

    @Override
    protected void processPacket(DatagramPacket packet) {
        List<Gossip> gossips = getGossips(packet);
        updateWorldView(gossips);
    }

    private void updateWorldView(List<Gossip> gossips) {
        gossips
                .stream()
                .map(gossip -> new Pair<>(peerTracker.add(gossip.getAboutPeer(), gossip.getStatus()), gossip))
                .forEach(pair -> {
                    Gossip gossip = pair.getValue();
                    if (pair.getKey().equals(ADD_STATUS.ADDED)) {
                        ADD_STATUS addStatus = gossipCollector.gossip(gossip);
                        LOGGER.info("Peer added {}. Gossip: {} Added? {}", gossip.getAboutPeer().logFriendly(), gossip.logFriendly(), addStatus);
                    }
                });
        if (otherPeersThinkIamDeadOrSuspicious(gossips)) {
            LOGGER.warn("I am suspected to be either dead or suspicious");
            gossipDisseminator.refuteMyStatus();
        }
    }

    private boolean otherPeersThinkIamDeadOrSuspicious(List<Gossip> gossips) {
        return gossips.stream().anyMatch(gossip ->
                gossip.getAboutPeer().equals(peerTracker.getIdentityPeer()) && !(gossip.getStatus().equals(Status.ALIVE)));
    }

    private List<Gossip> getGossips(DatagramPacket packet) {
        List<Gossip> gossips = new ArrayList<>();
        String gossipsAsString = getData(packet);
        String[] split = gossipsAsString.split("\n");
        for (String gossipAsString : split) {
            gossips.add(Gossip.fromString(gossipAsString));
        }

        return gossips;
    }

}
