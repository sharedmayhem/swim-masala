package com.simple_software.swim_masala.actor;

import com.simple_software.swim_masala.model.Gossip;
import com.simple_software.swim_masala.model.Peer;
import com.simple_software.swim_masala.model.Status;
import com.simple_software.swim_masala.supportactor.GossipCollector;
import com.simple_software.swim_masala.supportactor.IdentityPeerService;
import com.simple_software.swim_masala.supportactor.PeerTracker;
import com.simple_software.swim_masala.util.UDPCommunicator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Collections.singletonList;

/**
 * This class will disseminate gossip to other peers in a round robin fashion.
 */
public class GossipDisseminator {
    private static final Logger LOGGER = LoggerFactory.getLogger(GossipDisseminator.class);
    private final UDPCommunicator udpCommunicator;
    private final PeerTracker peerTracker;
    private final GossipCollector gossipCollector;
    private IdentityPeerService identityPeerService;

    public GossipDisseminator(UDPCommunicator udpCommunicator, PeerTracker peerTracker, GossipCollector gossipCollector, IdentityPeerService identityPeerService) {
        this.udpCommunicator = udpCommunicator;
        this.peerTracker = peerTracker;
        this.gossipCollector = gossipCollector;
        this.identityPeerService = identityPeerService;
    }


    public void disseminate() {
        List<Gossip> gossips = gossipCollector.allTheLatestGossips();
        if (gossips.size() > 0) {
            disseminate(peerTracker.getGossipPeers(), gossips);
            disseminateSuspiciousGossipsToSuspiciousPeers(gossips);
        }
        peerTracker.getADeadPeer().ifPresent(this::deliverKissOfLife);
    }

    private void disseminate(Collection<Peer> peers, List<Gossip> gossips) {
        String concatenatedGossips = Gossip.concatenatedGossips(gossips);
        peers.stream().parallel().forEach(peer -> udpCommunicator.send(peer.getHostname(), peer.getGossipReceiverPort(), concatenatedGossips));
    }

    void refuteMyStatus() {
        identityPeerService.newIncarnation();
        tellFactsAboutMyOwnWellBeing();
    }

    void tellFactsAboutMyOwnWellBeing() {
        Peer identityPeer = identityPeerService.identityPeer();
        Gossip myTruth = gossipCollector.getGossip(identityPeer, Status.ALIVE, identityPeer);
        Collection<Peer> gossipPeers = peerTracker.getGossipPeers();
        disseminate(gossipPeers, singletonList(myTruth));
        LOGGER.warn("I have let peers {} know that I am alive", gossipPeers.stream().map(Peer::logFriendly).collect(Collectors.joining(",")));
    }

    private void deliverKissOfLife(Peer deadPeer) {
        Gossip gossip = gossipCollector.getGossip(deadPeer, Status.DEAD, peerTracker.getIdentityPeer());
        disseminate(singletonList(deadPeer), singletonList(gossip));
        LOGGER.info("Delivered kiss of life to {}", deadPeer.logFriendly());
    }

    private void disseminateSuspiciousGossipsToSuspiciousPeers(List<Gossip> allGossips) {
        List<Gossip> suspiciousGossips = allGossips
                .stream()
                .filter(gossip -> gossip.getStatus().equals(Status.SUSPICIOUS))
                .collect(Collectors.toList());
        List<Peer> suspiciousPeers = suspiciousGossips.stream().map(Gossip::getAboutPeer).collect(Collectors.toList());
        disseminate(suspiciousPeers, suspiciousGossips);
    }
}
