
Ports:
 The implementation requires blocking a few ports for the purposes highlighted below:
   Acknowledger server
   Gossip Collector server

Actors:
 NodeClock
  ✔ Handles all time related queries of the node such as 'currentTimeMillis' @done (18-02-16 04:49)
  ✔ Is mocked in tests @done (18-02-16 04:49)
 Peer Tracker:
   ✔ Maintains the worldview of the node @done (18-02-10 10:54)
   ✔ Will provide a host in a round robin fashion to the pinger @done (18-02-10 10:54)
     ☐ Will sort the list once the pinger has gone through all its hosts
   ✔ Will provide a maximum of two hosts for the indirect pinger @done (18-02-10 10:55)
   ✔ Any update to the member tracker will return true if the status of the host has changed @done (18-02-10 10:55)
   ☐ Maintains dead peers in a seperate list

 Failure Detection:
   Failure Detector:
     ✔ Will trigger the pinger every round @done (18-02-10 10:52)
       ✔ If the pinger fails, triggers the indirect pinger @done (18-02-10 10:52)
       ✔ Will update results from the pinger/indirect pinger into member tracker @done (18-02-10 10:53)
         ✔ If member tracker indicates a status change, then update gossip collector @done (18-02-10 10:53)
       ✔ Will only gossip if the status of the peer has changed @done (18-02-12 06:07)
   Pinger:
     ✔ Pings the specified host and waits for a response for a specified period of time @done (18-02-17 21:32)
     ✔ Returns true if successful, false otherwise @done (18-02-17 21:32)
     ✔ Uses TCP @done (18-02-17 21:32)
   Indirect Pinger:
     ✔ Composed of multiple pingers @done (18-02-17 21:32)
     ✔ Will ping the multiple hosts in parallel and wait for response @done (18-02-17 21:32)
     ✔ Will return true if at least one host has a successful ping @done (18-02-17 21:32)
   Leaver:
       ☐ Will send a bye bye message to at least one peer via tcp to ensure guaranteed delivery
       ☐ Before sending the leaver message
           ☐ it will stop the ping receiver to prevent it from responding to any kind of ping requests
           ☐ it will stop the gossip receiver
           ☐ it will disseminate gossip about itself being dead once
   PingReceiver:
     ✔ Will respond to ping ack requests from other hosts @done (18-02-15 14:33)
     ✔ Will respond to indirect ping requests from other hosts @done (18-02-15 16:59)
         ✔ Will respond with status as alive if peer is reachable @done (18-02-15 16:59)
         ✔ Will respond with status as suspicious if peer is reachable @done (18-02-15 16:59)
     ✔ Will respond to messages from an infant with the entire world view @done (18-02-15 14:33)
           ✔ Will add the infant to peer tracker @done (18-02-15 15:09)
           ✔ Will add the infant to the gossip tracker @done (18-02-15 15:22)
       ✔ Will repond to leave messages @done (18-02-16 05:38)
   Gossip Collector:
     ✔ Will collect status from the failure detector and convert it into gossip @done (18-02-10 10:54)
     ✔ Will keep the gossips arranged in increasing order of age @done (18-02-10 10:54)
       ✔ Age here means the number of times the gossip has been gossipped @done (18-02-10 10:54)
     ✔ Will increment a gossip's age everytime it is gossipped @done (18-02-10 10:54)
     ✔ Will kill the oldest gossip @done (18-02-10 10:54)
     ✔ Will notify the gossip reviver when it kills a gossip @done (18-02-10 10:54)
     ✔ Will discard old gossip related to a node @done (18-02-10 10:54)
       ✔ Will use the incarnation number of a gossip @done (18-02-10 10:54)
     ✔ Will use the host's latest avatar and discard all messages of the host's old avatar @done (18-02-10 15:55)
       ✔ Each host will have to publish its local timestamp to indicate its 'avatar' @done (18-02-10 15:55)
     ✔ When there are no state changes in the cluster, it is possible that there is no gossip. Hence this cannot be used to represent the node's world view of the cluster @done (18-02-10 10:54)
     ✔ Will ignore all gossips except gossip related to a suspicious node @done (18-02-12 05:56)
     ✔ If the gossip was born 3 rounds (or any configurable number of rounds) before, then create new gossip that the host is dead and add to   gossip collector @done (18-02-12 05:56)
       ✔ Will update the member tracker that this host is dead @done (18-02-12 09:29)
   Gossip Receiver:
     ✔ Will listen to gossip from other peers @done (18-02-13 11:39)
       ✔ No need to react to gossip about oneself cause other hosts will eventually ping this host and realize that this host is alive @done (18-02-13 11:39)
       ✔ Will update the peer tracker @done (18-02-13 10:24)
       ✔ Will update the gossip collector only if status of peer has changed @done (18-02-13 10:24)
   Gossip Disseminator:
     ✔ Will dissemenate gossip to alive peers @done (18-02-13 05:26)
     ✔ Will run more often than the failure detector @done (18-02-17 10:13)
     ✔ Will disseminate suspicious gossip to the suspicious peer to give it a chance to refute its status
     It's main aim is to gossip as quickly as possible and exhaust the gossip collector
   Infant:
     ✔ Will send 'first gossip' (hi message) to the ping receiver @done (18-02-17 18:08)
     ✔ Will update its peer tracker and gossip collector on receiving input @done (18-02-17 18:08)
   Chaos Monkey:
     - Is a malicious peer that will send the wrong messages about other peers or itself 


Concerns/Improvements:
 - There are scenarios where gossip reviver's message might be overridden with yet another suspicious message
 network partitions
    ☐ keep the dead nodes in a buffer
    ☐ distinguish between voluntary leaving and sudden death
 ☐ New node joining a large cluster. How long will it take for the new node to get to know about everyone else?
 ☐ Security?
 ☐ Compression of messages
 ☐ Use lampport clocks instead of incarnation number?

q) What message types can a peer receive?
a) A peer can receive the following messages in the following modes:
  i) Failure detector-
     an indirect ping request-
       A request from another peer asking this peer to ping other peers
     ack request-
       A request from another peer to acknowledge this peer's liveness 
  ii) Gossip Receiver-
     status of other nodes in the format: <node_name>:<status>:<incarnation_number>

q) What message types can a peer send?
a) A peer can send the following types of messages in the following modes-
  i) Bootstrap (when incarnation number is 1 and source of the packet matches the host in the packet data)-
  ii) Failure detector-
  iii) Gossip disseminator-