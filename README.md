# Swim Masala - A Group Membership Protocol
This is an implementation of the SWIM protocol. The paper is [here]. 

## How do I get set up? ###
* ##### Dependencies
    * Java 8
    * Maven
    * Bash
    * AWS (if you want to use the provisioning scripts)
* ##### Set up and run on AWS
    * Clone project
    * mvn clean package
    * Navigate to scripts directory at the root of the project
    * Open file *machines.txt* and setup machine types (Jump host, Parent and ClusterMember)
        - Jump host is an EC2 instance that has a public IP you can ssh into from your local machine. 
        - Parent is an EC2 instance where the first peer is started and all the other peers that later start will talk to this peer to register themselves
        - ClusterMember is an EC2 instance which will host peers 
    * Run *provision.sh* by providing the AWS pem file as an argument
    * At this point, the newly built libraries are deployed on the AWS infrastructure and the cluster would be up and running
* ##### Set up and run on local
    * mvn clean install
    * Run the program 'GossipProtocol' with the VM arguments: -Dpeer-name=parent -Dtest-mode=true. This will bring up the first peer listening to pings on port 60000
    * Run as many instance of the the program 'GossipProtocol' with the VM arguments: -Dpeer-name=<friendly peer name> -Dparent=<hostname/IP address of first peer> -Dport=60000. This will bring up peers.

## Overview of the algorithm
- Upon startup, a peer A talks to an introducer node (can be any member of the cluster) to receive the introducer’s node entire world view (its peers and statuses) to build it’s own world view.
- The peer has multiple threads running in the background to:
	- #### Detect failure
		- A thread will periodically ping a peer from its peer tracking list in a round robin fashion. 
		- If the peer is not reachable directly or indirectly via other peers, then the peer is marked suspicious else it is deemed alive
	- #### Disseminate Gossip
		- If the failure detector detects a status change for a peer in its rounds, then this is disseminated as gossip to other peers, picked off the peer tracker list in a round robin fashion. 
		- If a gossip has been gossiped to other peers a configurable number of rounds, then it is ‘aged’ out and discarded.
	- #### Receive Gossip
		- Gossips received from other peers is stored and passed onto the gossip disseminator. 
		- If the peer receives either suspicious or dead gossip about itself, then it attempts to refute it by disseminating its status to other peers via the gossip disseminator.
			- In order to trump the older gossip messages about its’ suspicious/dead state that might be still passed around in the cluster, the peer assumes a new incarnation of itself and sends gossip about its well being using this incarnation. Other peers will prefer gossip messages of the latest peer incarnation.
- Peers in its world view can have either of the following states
	- #### ALIVE
		- A peer is alive if the failure detector can ping it. If otherwise, then the failure detector picks two other peers from its list and asks those peers to ping it. If even this indirect ping fails, then the peer is deemed suspicious.
	- #### SUSPICIOUS
		- An unreachable peer is not immediately marked dead because the problem regarding the peer may be temporary. For e.g., the network switch between peer A and peer B might have been offline for a few seconds during its reboot. Hence peer A can reach peer B post reboot.
		- A suspicious peer is treated normally by other peers i.e., it is not removed from the peer tracking lists. It will continue to receive gossip and failure detection pings; thus giving the peer an opportunity to refute its suspicious status in the cluster.  
	- #### DEAD
		- A peer is marked as dead if it has been in a suspicious state for a configurable number of gossip rounds. 
		- A dead peer is ignored by all peers and kept in a separate list till a certain timeout is reached.
		- On reaching this timeout, a peer will attempt to resuscitate the dead peer by sending it a final gossip message about itself, giving it a chance to refute its status in case it is alive. The dead peer is removed from the dead peer tracker after this. 	

## Measurements
### Assumptions
- Failure detector running once every second
- Gossip disseminator running once every 200ms and gossip age lasting 2 rounds i.e., 400ms
#### 4 machine cluster background bandwidth (number of messages per second)
- #### Stationary membership
	- 1 message per request and 1 per response per host => 8 messages/sec
- #### Perturbed cluster
	- JOIN: 2 messages for every peer join + 6 gossip messages
	- LEAVE: 2 messages for every peer leave + 6 gossip messages 
	- DEAD: 6 gossip messages 
### False Positives
#### Assumptions
- Loss is simulated at a peer by dropping incoming messages continuously until the probability goals are met. For e.g., if loss probability is 0.02, then the lossy receivers will process 98 messages and drop the next two. This could be implemented better by interspersing the loss more randomly across the input.
#### 3 member cluster with a lossy probability of 0.02
- 3 minutes before a peer marked the lossy peer as suspicious. Time to correction was not measurable since it was instant
#### 3 member cluster with a lossy probability of 0.10
- 1 minute 45 seconds before a peer marked the lossy peer as suspicious. Time to correction was 3 seconds
#### 3 member cluster with a lossy probability of 0.30
- 1 minute 32 seconds before a peer marked the lossy peer as suspicious. Time to correction was 18 seconds
#### 4 member cluster with a lossy probability of 0.02
- 3 minutes before a peer marked the lossy peer as suspicious. Time to correction was not measurable since it was instant
#### 4 member cluster with a lossy probability of 0.10
- 2 minutes 30 seconds before a peer marked the lossy peer as suspicious. Time to correction was 1 second
#### 4 member cluster with a lossy probability of 0.30
- 2 minutes 10 seconds before a peer marked the lossy peer as suspicious. Time to correction was 20 seconds
