#!/bin/sh
file="machines.txt"
pem_file="distributed-grep.pem.txt"

# *** Variables 'pem_file' and 'host' declared in this script are used in common.sh ***
. ./common.sh

while IFS= read -r line
do
    IFS=':' read -ra machine_info <<< "$line"
    machine_type=${machine_info[0]}
    host=${machine_info[1]}
     if [ $machine_type = 'Parent' ] || [ $machine_type = 'ClusterMember' ]; then
        do_scp 'runOnPeer.sh' '/home/ec2-user/runOnPeer.sh'
        do_ssh 'chmod +x /home/ec2-user/runOnPeer.sh'
        do_ssh './runOnPeer.sh'
        do_scp 'swim*.jar' '/home/ec2-user'
     fi
done <"$file"

count=0
while IFS= read -r line
do
    IFS=':' read -ra machine_info <<< "$line"
    machine_type=${machine_info[0]}
    host=${machine_info[1]}
    if [ $machine_type = 'Parent' ] || [ $machine_type = 'ClusterMember' ]; then
        echo "Provisioning host: $host"
        if [ $machine_type = 'Parent' ]; then
            echo "Parent"
            parent_ip=${host}
            name=parent
            command="java -Dpeer-name=${name} -Dtest-mode=true -cp /home/ec2-user/swim-masala.jar com.simple_software.swim_masala.GossipProtocol &>/dev/null &"
            do_ssh "nohup ${command}"
        elif [ $machine_type = 'ClusterMember' ]; then
            echo "Child"
            for i in {1..2}
            do
                ((++count))
                name=child${count}
                command="java -Dpeer-name=${name} -Dparent=${parent_ip} -Dport=60000 -cp /home/ec2-user/swim-masala.jar com.simple_software.swim_masala.GossipProtocol &>/dev/null &"
                do_ssh "nohup ${command}"
            done
            echo "Children started on host: $host"
        fi

    fi
done <"$file"


